﻿namespace Gymtesis
{
    partial class ABMProf
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ABMProf));
            this.DGVProf = new System.Windows.Forms.DataGridView();
            this.btbusprof = new System.Windows.Forms.Button();
            this.btnewprof = new System.Windows.Forms.Button();
            this.btmodprof = new System.Windows.Forms.Button();
            this.bteliprof = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtdniprof = new System.Windows.Forms.TextBox();
            this.btsalir = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.bsourceprof = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.DGVProf)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsourceprof)).BeginInit();
            this.SuspendLayout();
            // 
            // DGVProf
            // 
            this.DGVProf.AllowUserToAddRows = false;
            this.DGVProf.AllowUserToDeleteRows = false;
            this.DGVProf.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVProf.Location = new System.Drawing.Point(12, 147);
            this.DGVProf.Name = "DGVProf";
            this.DGVProf.ReadOnly = true;
            this.DGVProf.Size = new System.Drawing.Size(808, 257);
            this.DGVProf.TabIndex = 0;
            // 
            // btbusprof
            // 
            this.btbusprof.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(104)))), ((int)(((byte)(0)))));
            this.btbusprof.FlatAppearance.BorderSize = 0;
            this.btbusprof.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btbusprof.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btbusprof.Location = new System.Drawing.Point(379, 64);
            this.btbusprof.Name = "btbusprof";
            this.btbusprof.Size = new System.Drawing.Size(87, 26);
            this.btbusprof.TabIndex = 1;
            this.btbusprof.Text = "Buscar";
            this.btbusprof.UseVisualStyleBackColor = false;
            this.btbusprof.Click += new System.EventHandler(this.btbusprof_Click);
            // 
            // btnewprof
            // 
            this.btnewprof.FlatAppearance.BorderSize = 0;
            this.btnewprof.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnewprof.Image = ((System.Drawing.Image)(resources.GetObject("btnewprof.Image")));
            this.btnewprof.Location = new System.Drawing.Point(14, 141);
            this.btnewprof.Name = "btnewprof";
            this.btnewprof.Size = new System.Drawing.Size(150, 57);
            this.btnewprof.TabIndex = 2;
            this.btnewprof.Text = "Nuevo profesional";
            this.btnewprof.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnewprof.UseVisualStyleBackColor = true;
            this.btnewprof.Click += new System.EventHandler(this.btnewprof_Click);
            // 
            // btmodprof
            // 
            this.btmodprof.FlatAppearance.BorderSize = 0;
            this.btmodprof.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmodprof.Image = ((System.Drawing.Image)(resources.GetObject("btmodprof.Image")));
            this.btmodprof.Location = new System.Drawing.Point(14, 204);
            this.btmodprof.Name = "btmodprof";
            this.btmodprof.Size = new System.Drawing.Size(150, 57);
            this.btmodprof.TabIndex = 3;
            this.btmodprof.Text = "Modificar";
            this.btmodprof.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btmodprof.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btmodprof.UseVisualStyleBackColor = true;
            this.btmodprof.Click += new System.EventHandler(this.btmodprof_Click);
            // 
            // bteliprof
            // 
            this.bteliprof.FlatAppearance.BorderSize = 0;
            this.bteliprof.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bteliprof.Image = ((System.Drawing.Image)(resources.GetObject("bteliprof.Image")));
            this.bteliprof.Location = new System.Drawing.Point(14, 267);
            this.bteliprof.Name = "bteliprof";
            this.bteliprof.Size = new System.Drawing.Size(150, 57);
            this.bteliprof.TabIndex = 4;
            this.bteliprof.Text = "Eliminar";
            this.bteliprof.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bteliprof.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.bteliprof.UseVisualStyleBackColor = true;
            this.bteliprof.Click += new System.EventHandler(this.bteliprof_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(180, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "DNI";
            // 
            // txtdniprof
            // 
            this.txtdniprof.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdniprof.Location = new System.Drawing.Point(223, 64);
            this.txtdniprof.Name = "txtdniprof";
            this.txtdniprof.Size = new System.Drawing.Size(160, 26);
            this.txtdniprof.TabIndex = 6;
            // 
            // btsalir
            // 
            this.btsalir.FlatAppearance.BorderSize = 0;
            this.btsalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btsalir.Image = ((System.Drawing.Image)(resources.GetObject("btsalir.Image")));
            this.btsalir.Location = new System.Drawing.Point(14, 361);
            this.btsalir.Name = "btsalir";
            this.btsalir.Size = new System.Drawing.Size(150, 53);
            this.btsalir.TabIndex = 7;
            this.btsalir.Text = "Salir";
            this.btsalir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btsalir.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btsalir.UseVisualStyleBackColor = true;
            this.btsalir.Click += new System.EventHandler(this.btsalir_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(104)))), ((int)(((byte)(0)))));
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btnewprof);
            this.panel1.Controls.Add(this.btsalir);
            this.panel1.Controls.Add(this.btmodprof);
            this.panel1.Controls.Add(this.bteliprof);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(826, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(164, 426);
            this.panel1.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(38, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "Profesionales";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(41, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(101, 83);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(-3, 345);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(171, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "-----------------------------------------";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // ABMProf
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(990, 426);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtdniprof);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btbusprof);
            this.Controls.Add(this.DGVProf);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ABMProf";
            this.Text = "ABMProf";
            this.Load += new System.EventHandler(this.ABMProf_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGVProf)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsourceprof)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DGVProf;
        private System.Windows.Forms.Button btbusprof;
        private System.Windows.Forms.Button btnewprof;
        private System.Windows.Forms.Button btmodprof;
        private System.Windows.Forms.Button bteliprof;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtdniprof;
        private System.Windows.Forms.Button btsalir;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.BindingSource bsourceprof;
    }
}