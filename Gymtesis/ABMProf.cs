﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//
using ConectorBD;
using System.Configuration;


namespace Gymtesis
{
    public partial class ABMProf : Form
    {
        DatosMySQL objDatos;
        DataTable dt;
        string cadCn;      
        public ABMProf()
        {
            InitializeComponent();
        }
        private void ABMProf_Load(object sender, EventArgs e)
        {
            cadCn = ConfigurationManager.ConnectionStrings["MYSQL"].ConnectionString;
  
                //bsourceprof.DataSource = dt;
                DGVProf.DataSource = bsourceprof;
            
                actualizarprof(txtdniprof.Text);
        }
        private void actualizarprof(string prf)
        {
            string profSQL = ApTables.actfesionales(prf);
            objDatos = new DatosMySQL();
            dt = new DataTable();

            dt = objDatos.traerDataTable(profSQL, cadCn);

            if (dt == null || dt.Rows.Count == 0)
                bsourceprof.DataSource = null;
            else
                bsourceprof.DataSource = dt;
        }

        private void btnewprof_Click(object sender, EventArgs e)
        {
            AgregarProf aprof = new AgregarProf();
            aprof.ShowDialog();
            txtdniprof.Text = "";
            actualizarprof(txtdniprof.Text);
            voldgv();
        }

        private void btmodprof_Click(object sender, EventArgs e)
        {
            Modprof moprof = new Modprof();
            moprof.idmod = DGVProf.CurrentRow.Cells[0].Value.ToString();
            txtdniprof.Text = "";           
            moprof.ShowDialog();
            actualizarprof(txtdniprof.Text);
            voldgv();
        }
       
        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void bteliprof_Click(object sender, EventArgs e)
        {
            var nroid = DGVProf.CurrentRow.Cells["ID"].Value;
            var nomape = DGVProf.CurrentRow.Cells["Nombre y Apellido"].Value;
            var dni = DGVProf.CurrentRow.Cells["DNI"].Value;
            string elimprof = ApTables.eliminarprofesionales(dni.ToString());
            int valordev = -1;
            objDatos = new DatosMySQL();

            string msg = "Desea eliminar al profesional " + Environment.NewLine + "DNI: " + dni + Environment.NewLine + "Nombre: " + nomape;

            DialogResult ok = MessageBox.Show(msg, "Borrar", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if(ok==System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    valordev = objDatos.ejecutarSQL(elimprof, cadCn);
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                if (valordev > 0)
                    actualizarprof(txtdniprof.Text);
                voldgv();
            }
        }
        private void busdni()
        {
            DataView dv = dt.DefaultView;

            if (txtdniprof.Text.Trim() != "")
                dv.RowFilter = "dni=" + txtdniprof.Text;
            else
                dv.RowFilter = "";
            DGVProf.DataSource = dv;
        }
        private void voldgv()
        {
            DataView dv = dt.DefaultView;
            DGVProf.DataSource = dv;
        }
        private void btsalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btbusprof_Click(object sender, EventArgs e)
        {
            busdni();
        }
    }
}
