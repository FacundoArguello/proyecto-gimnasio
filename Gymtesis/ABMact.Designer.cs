﻿namespace Gymtesis
{
    partial class ABMact
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ABMact));
            this.txtnomact = new System.Windows.Forms.TextBox();
            this.txtprecio = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnewact = new System.Windows.Forms.Button();
            this.btmodact = new System.Windows.Forms.Button();
            this.btelimact = new System.Windows.Forms.Button();
            this.btcanact = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtcapacidad = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtnomact
            // 
            this.txtnomact.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtnomact.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnomact.Location = new System.Drawing.Point(26, 48);
            this.txtnomact.Name = "txtnomact";
            this.txtnomact.Size = new System.Drawing.Size(105, 22);
            this.txtnomact.TabIndex = 0;
            // 
            // txtprecio
            // 
            this.txtprecio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprecio.Location = new System.Drawing.Point(166, 48);
            this.txtprecio.MaxLength = 6;
            this.txtprecio.Name = "txtprecio";
            this.txtprecio.Size = new System.Drawing.Size(105, 22);
            this.txtprecio.TabIndex = 3;
            this.txtprecio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtprecio_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(57, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Nombre";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(26, 102);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(410, 197);
            this.dataGridView1.TabIndex = 8;
            // 
            // btnewact
            // 
            this.btnewact.FlatAppearance.BorderSize = 0;
            this.btnewact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnewact.Image = ((System.Drawing.Image)(resources.GetObject("btnewact.Image")));
            this.btnewact.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnewact.Location = new System.Drawing.Point(13, 102);
            this.btnewact.Name = "btnewact";
            this.btnewact.Size = new System.Drawing.Size(107, 48);
            this.btnewact.TabIndex = 9;
            this.btnewact.Text = "Nueva actividad";
            this.btnewact.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnewact.UseVisualStyleBackColor = true;
            this.btnewact.Click += new System.EventHandler(this.btnewact_Click);
            // 
            // btmodact
            // 
            this.btmodact.FlatAppearance.BorderSize = 0;
            this.btmodact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmodact.Image = ((System.Drawing.Image)(resources.GetObject("btmodact.Image")));
            this.btmodact.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btmodact.Location = new System.Drawing.Point(13, 156);
            this.btmodact.Name = "btmodact";
            this.btmodact.Size = new System.Drawing.Size(107, 48);
            this.btmodact.TabIndex = 10;
            this.btmodact.Text = "Modificar actividad";
            this.btmodact.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btmodact.UseVisualStyleBackColor = true;
            this.btmodact.Click += new System.EventHandler(this.btmodact_Click);
            // 
            // btelimact
            // 
            this.btelimact.FlatAppearance.BorderSize = 0;
            this.btelimact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btelimact.Image = ((System.Drawing.Image)(resources.GetObject("btelimact.Image")));
            this.btelimact.Location = new System.Drawing.Point(13, 210);
            this.btelimact.Name = "btelimact";
            this.btelimact.Size = new System.Drawing.Size(107, 48);
            this.btelimact.TabIndex = 11;
            this.btelimact.Text = "Eliminar actividad";
            this.btelimact.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btelimact.UseVisualStyleBackColor = true;
            this.btelimact.Click += new System.EventHandler(this.btelimact_Click);
            // 
            // btcanact
            // 
            this.btcanact.FlatAppearance.BorderSize = 0;
            this.btcanact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btcanact.Image = ((System.Drawing.Image)(resources.GetObject("btcanact.Image")));
            this.btcanact.Location = new System.Drawing.Point(13, 264);
            this.btcanact.Name = "btcanact";
            this.btcanact.Size = new System.Drawing.Size(107, 48);
            this.btcanact.TabIndex = 12;
            this.btcanact.Text = "Cancelar";
            this.btcanact.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btcanact.UseVisualStyleBackColor = true;
            this.btcanact.Click += new System.EventHandler(this.btcanact_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(21)))), ((int)(((byte)(34)))));
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.btcanact);
            this.panel1.Controls.Add(this.btelimact);
            this.panel1.Controls.Add(this.btmodact);
            this.panel1.Controls.Add(this.btnewact);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.ForeColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(473, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(132, 335);
            this.panel1.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(24, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 16);
            this.label5.TabIndex = 14;
            this.label5.Text = "Actividad";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(2, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(121, 80);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(200, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 16);
            this.label6.TabIndex = 15;
            this.label6.Text = "Precio";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(324, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 16);
            this.label2.TabIndex = 17;
            this.label2.Text = "Capacidad";
            // 
            // txtcapacidad
            // 
            this.txtcapacidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcapacidad.Location = new System.Drawing.Point(305, 48);
            this.txtcapacidad.MaxLength = 6;
            this.txtcapacidad.Name = "txtcapacidad";
            this.txtcapacidad.Size = new System.Drawing.Size(105, 22);
            this.txtcapacidad.TabIndex = 16;
            this.txtcapacidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcapacidad_KeyPress);
            // 
            // ABMact
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(605, 335);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtcapacidad);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtprecio);
            this.Controls.Add(this.txtnomact);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ABMact";
            this.Text = "ABMact";
            this.Load += new System.EventHandler(this.ABMact_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtnomact;
        private System.Windows.Forms.TextBox txtprecio;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnewact;
        private System.Windows.Forms.Button btmodact;
        private System.Windows.Forms.Button btelimact;
        private System.Windows.Forms.Button btcanact;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtcapacidad;
    }
}