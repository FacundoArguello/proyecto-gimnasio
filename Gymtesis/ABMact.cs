﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//
using ConectorBD;
using System.Configuration;

namespace Gymtesis
{
    public partial class ABMact : Form
    {
        DatosMySQL objDatos;
        DataTable dt;
        string cadCn;
        
        public ABMact()
        {
            InitializeComponent();
        }

        private void btmodact_Click(object sender, EventArgs e)
        {
            ModAct mact = new ModAct();
            mact.idact = dataGridView1.CurrentRow.Cells[0].Value.ToString();           
            mact.ShowDialog();
            txtnomact.Text = "";
            obteneract();
        }

        private void ABMact_Load(object sender, EventArgs e)
        {
            cadCn = ConfigurationManager.ConnectionStrings["MYSQL"].ConnectionString;
            bindingSource1.DataSource = dt;
            dataGridView1.DataSource = bindingSource1;
            obteneract();
            //obxprof();
        }
        private void obteneract()
        {
            string actSQL = ApTables.BDactividades();
            objDatos = new DatosMySQL();
            dt = new DataTable();

            dt = objDatos.traerDataTable(actSQL, cadCn);

            if (dt == null || dt.Rows.Count == 0)
                bindingSource1.DataSource = null;
            else
                bindingSource1.DataSource = dt;
        }
        private bool validarcarga()
        {
            if(txtnomact.Text=="")
            {
                MessageBox.Show("Debe ingresar el nombre de la actividad");
                txtnomact.Focus();
                return false;
            }
            if(txtprecio.Text=="")
            {
                MessageBox.Show("Debe ingresar el precio de la actividad");
                txtprecio.Focus();
                return false;
            }
            if (txtcapacidad.Text == "")
            {
                MessageBox.Show("Debe ingresar la capacidad de la actividad");
                txtcapacidad.Focus();
                return false;
            }
            return true;
                
        }
        private void btnewact_Click(object sender, EventArgs e)
        {
            if (validarcarga() == true)
            {
                string agractSQL = "insert into gimnasio.actividad (nomact, precio, capacidad) values('"
                    + txtnomact.Text + "','" + "$" + txtprecio.Text + "'," + txtcapacidad.Text + ")";
                objDatos = new DatosMySQL();
                int valordev = -1;

                DialogResult ok = MessageBox.Show("Agregar", "Desea agregar una nueva actividad", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (ok == System.Windows.Forms.DialogResult.Yes)
                {
                    try
                    {
                        valordev = objDatos.ejecutarSQL(agractSQL, cadCn);
                        txtnomact.Text = "";
                        txtprecio.Text = "";
                        txtcapacidad.Text = "";
                        //mtxthor.Text = "";

                    }
                    catch (SystemException ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    if (valordev > 0)
                        obteneract();
                }
            }
        }
        
        private void btcanact_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btelimact_Click(object sender, EventArgs e)
        {
            txtnomact.Text = "";
            var act = dataGridView1.CurrentRow.Cells["ID"].Value;
            var nomact = dataGridView1.CurrentRow.Cells["Nombre"].Value;
            string elimASQL = ApTables.BDeliminaract(act.ToString());
            objDatos = new DatosMySQL();
            int valordev = -1;

            string msg = "Desea eliminar la actividad : " + nomact;
            DialogResult oka = MessageBox.Show(msg,"Eliminar actividad", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            
            if(oka== System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    valordev = objDatos.ejecutarSQL(elimASQL, cadCn);
                }
                catch(SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                if (valordev > 0)
                    obteneract();
            }
        }

        private void txtprecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtcapacidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
               if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
