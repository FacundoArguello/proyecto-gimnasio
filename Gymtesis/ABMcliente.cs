﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//
using ConectorBD;
using System.Configuration;

namespace Gymtesis
{
    public partial class ABMcliente : Form
    {
        DatosMySQL objDatos;
        DataTable dt;
        string cadCn;
        public ABMcliente()
        {
            InitializeComponent();
        }
        Agregarcliente agc = new Agregarcliente();
        Modificarcliente modc = new Modificarcliente();
        private void ABMcliente_Load(object sender, EventArgs e)
        {
            cadCn = ConfigurationManager.ConnectionStrings["MYSQL"].ConnectionString;
            if (obtenerClientes() != null)
            {

                bindingSource2.DataSource = dt;
                dataGridView1.DataSource = bindingSource2;
            }
            actxclientes(textdni.Text);           
        }

        private DataTable obtenerClientes()
        {
            string clSQL = ApTables.obclientes();
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(clSQL, cadCn);
            if(dt==null || dt.Rows.Count == 0) return null;
            return dt;
        }
        private void actxclientes(string actu)
        {
            string acxSQL = ApTables.actualizarclientes(actu);
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(acxSQL, cadCn);
            if (dt == null || dt.Rows.Count == 0)
                bindingSource2.DataSource = null;
            else
                bindingSource2.DataSource = dt;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
           
        }

        private void button4_Click_1(object sender, EventArgs e)
        {   
            modc.dni = dataGridView1.CurrentRow.Cells[0].Value.ToString(); 
            modc.ShowDialog();
            textdni.Text = "";

            actxclientes(textdni.Text);
            volverdgv();
        }
      
        private void button5_Click(object sender, EventArgs e)
        { 
            textdni.Text = " ";
            var nroid = dataGridView1.CurrentRow.Cells["ID"].Value;
            var nrodni = dataGridView1.CurrentRow.Cells["DNI"].Value;
            var nom = dataGridView1.CurrentRow.Cells["Nombre"].Value;
            string elimSQL = ApTables.eliminarcliente(nrodni.ToString());
            objDatos = new DatosMySQL();
            int valordev = -1;

            string msg = "Desea eliminar al cliente " + Environment.NewLine + "DNI : " + nrodni + Environment.NewLine + "Nombre : " + nom;

            DialogResult ok = MessageBox.Show(msg, "Borrar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            
            if (ok == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    valordev = objDatos.ejecutarSQL(elimSQL, cadCn);
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                if (valordev > 0)
                    actxclientes(textdni.Text);// hacer el actualizador
                volverdgv();
                
            }
        }

        private void btagregar_Click(object sender, EventArgs e)
        {
            agc.ShowDialog();
            textdni.Text = "";
            actxclientes(textdni.Text);
          
        }

        private void btcancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void obexpiracion()
        {
            string feSQL = "select dni,datediff(feexp, curdate()) AS expiracion from gimnasio.cliente";
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(feSQL, cadCn);

            
                foreach (DataGridViewRow fila in dataGridView1.Rows)
                {
                    if (Convert.ToInt32(fila.Cells[1].Value) < 0)
                    {
                        fila.DefaultCellStyle.BackColor = Color.IndianRed;
                    }

                    else if (Convert.ToInt32(fila.Cells[1].Value) == 0)
                    {

                        fila.DefaultCellStyle.BackColor = Color.GreenYellow;
                    }

                    else
                    {
                        fila.DefaultCellStyle.BackColor = Color.LimeGreen;
                    }
                }
                                
        }
        private void buscardni()
        {
            DataView dv = dt.DefaultView;

            if (textdni.Text.Trim() != "")
                dv.RowFilter = "dni=" + textdni.Text;
            else
                dv.RowFilter = "";

            dataGridView1.DataSource = dv;
            
        }
        private void volverdgv()
        {
            DataView dv = dt.DefaultView;
            dataGridView1.DataSource = dv;
        }
        private void btbuscar_Click(object sender, EventArgs e)
        {        
            buscardni();
            textdni.Text = "";
        }

        private void textdni_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))//Para obligar a que sólo se introduzcan números
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar))//permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;//el resto de teclas pulsadas se desactivan
            }
        
        }
    }
}
