﻿namespace Gymtesis
{
    partial class AgregarProf
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AgregarProf));
            this.cbactpro = new System.Windows.Forms.ComboBox();
            this.txtmail = new System.Windows.Forms.TextBox();
            this.txtdirpro = new System.Windows.Forms.TextBox();
            this.txttelpro = new System.Windows.Forms.TextBox();
            this.txtdniprof = new System.Windows.Forms.TextBox();
            this.txtnomape = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpfn = new System.Windows.Forms.DateTimePicker();
            this.txtnro = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cbloc = new System.Windows.Forms.ComboBox();
            this.cbprov = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btagregarpro = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbdia = new System.Windows.Forms.ComboBox();
            this.cbhorp = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbactpro
            // 
            this.cbactpro.FormattingEnabled = true;
            this.cbactpro.Location = new System.Drawing.Point(108, 406);
            this.cbactpro.Name = "cbactpro";
            this.cbactpro.Size = new System.Drawing.Size(137, 21);
            this.cbactpro.TabIndex = 32;
            this.cbactpro.SelectedIndexChanged += new System.EventHandler(this.cbactpro_SelectedIndexChanged);
            // 
            // txtmail
            // 
            this.txtmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmail.Location = new System.Drawing.Point(149, 345);
            this.txtmail.Name = "txtmail";
            this.txtmail.Size = new System.Drawing.Size(183, 20);
            this.txtmail.TabIndex = 30;
            this.txtmail.Leave += new System.EventHandler(this.txtmail_Leave);
            // 
            // txtdirpro
            // 
            this.txtdirpro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdirpro.Location = new System.Drawing.Point(149, 246);
            this.txtdirpro.Name = "txtdirpro";
            this.txtdirpro.Size = new System.Drawing.Size(183, 20);
            this.txtdirpro.TabIndex = 28;
            // 
            // txttelpro
            // 
            this.txttelpro.Location = new System.Drawing.Point(149, 139);
            this.txttelpro.MaxLength = 15;
            this.txttelpro.Name = "txttelpro";
            this.txttelpro.Size = new System.Drawing.Size(183, 20);
            this.txttelpro.TabIndex = 27;
            this.txttelpro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txttelpro_KeyPress);
            // 
            // txtdniprof
            // 
            this.txtdniprof.Location = new System.Drawing.Point(149, 93);
            this.txtdniprof.MaxLength = 8;
            this.txtdniprof.Name = "txtdniprof";
            this.txtdniprof.Size = new System.Drawing.Size(183, 20);
            this.txtdniprof.TabIndex = 26;
            this.txtdniprof.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtdniprof_KeyPress);
            this.txtdniprof.Validating += new System.ComponentModel.CancelEventHandler(this.txtdniprof_Validating);
            // 
            // txtnomape
            // 
            this.txtnomape.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtnomape.Location = new System.Drawing.Point(149, 42);
            this.txtnomape.Name = "txtnomape";
            this.txtnomape.Size = new System.Drawing.Size(183, 20);
            this.txtnomape.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(286, 389);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Dias";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(87, 348);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Mail";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(42, 406);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Actividad";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Fecha de nacimiento";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(64, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Telefono";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(67, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "DNI";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 245);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Direccion";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Nombre y apellido";
            // 
            // dtpfn
            // 
            this.dtpfn.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpfn.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dtpfn.Location = new System.Drawing.Point(149, 197);
            this.dtpfn.MaxDate = new System.DateTime(2017, 12, 31, 0, 0, 0, 0);
            this.dtpfn.MinDate = new System.DateTime(1930, 1, 1, 0, 0, 0, 0);
            this.dtpfn.Name = "dtpfn";
            this.dtpfn.Size = new System.Drawing.Size(200, 20);
            this.dtpfn.TabIndex = 33;
            // 
            // txtnro
            // 
            this.txtnro.Location = new System.Drawing.Point(404, 246);
            this.txtnro.MaxLength = 5;
            this.txtnro.Name = "txtnro";
            this.txtnro.Size = new System.Drawing.Size(82, 20);
            this.txtnro.TabIndex = 34;
            this.txtnro.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            this.txtnro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnro_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(343, 249);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 13);
            this.label9.TabIndex = 35;
            this.label9.Text = "Numero";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(294, 297);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.TabIndex = 36;
            this.label10.Text = "Localidad";
            // 
            // cbloc
            // 
            this.cbloc.FormattingEnabled = true;
            this.cbloc.Location = new System.Drawing.Point(365, 294);
            this.cbloc.Name = "cbloc";
            this.cbloc.Size = new System.Drawing.Size(121, 21);
            this.cbloc.TabIndex = 37;
            this.cbloc.SelectedIndexChanged += new System.EventHandler(this.cbloc_SelectedIndexChanged);
            // 
            // cbprov
            // 
            this.cbprov.FormattingEnabled = true;
            this.cbprov.Location = new System.Drawing.Point(149, 294);
            this.cbprov.Name = "cbprov";
            this.cbprov.Size = new System.Drawing.Size(121, 21);
            this.cbprov.TabIndex = 38;
            this.cbprov.SelectedIndexChanged += new System.EventHandler(this.cbprov_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(77, 297);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 13);
            this.label11.TabIndex = 39;
            this.label11.Text = "Provincia";
            // 
            // btagregarpro
            // 
            this.btagregarpro.FlatAppearance.BorderSize = 0;
            this.btagregarpro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btagregarpro.Image = ((System.Drawing.Image)(resources.GetObject("btagregarpro.Image")));
            this.btagregarpro.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btagregarpro.Location = new System.Drawing.Point(108, 15);
            this.btagregarpro.Name = "btagregarpro";
            this.btagregarpro.Size = new System.Drawing.Size(130, 49);
            this.btagregarpro.TabIndex = 40;
            this.btagregarpro.Text = "Agregar";
            this.btagregarpro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btagregarpro.UseVisualStyleBackColor = true;
            this.btagregarpro.Click += new System.EventHandler(this.btagregarpro_Click);
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(264, 15);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(123, 49);
            this.button2.TabIndex = 41;
            this.button2.Text = "Cancelar";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(104)))), ((int)(((byte)(0)))));
            this.panel1.Controls.Add(this.btagregarpro);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 460);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(509, 67);
            this.panel1.TabIndex = 42;
            // 
            // cbdia
            // 
            this.cbdia.FormattingEnabled = true;
            this.cbdia.Location = new System.Drawing.Point(346, 386);
            this.cbdia.Name = "cbdia";
            this.cbdia.Size = new System.Drawing.Size(121, 21);
            this.cbdia.TabIndex = 43;
            this.cbdia.SelectedIndexChanged += new System.EventHandler(this.cbdia_SelectedIndexChanged);
            // 
            // cbhorp
            // 
            this.cbhorp.FormattingEnabled = true;
            this.cbhorp.Location = new System.Drawing.Point(346, 427);
            this.cbhorp.Name = "cbhorp";
            this.cbhorp.Size = new System.Drawing.Size(121, 21);
            this.cbhorp.TabIndex = 44;
            this.cbhorp.SelectedIndexChanged += new System.EventHandler(this.cbhorp_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(286, 430);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 13);
            this.label12.TabIndex = 45;
            this.label12.Text = "Horario";
            // 
            // AgregarProf
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(509, 527);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.cbhorp);
            this.Controls.Add(this.cbdia);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cbprov);
            this.Controls.Add(this.cbloc);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtnro);
            this.Controls.Add(this.dtpfn);
            this.Controls.Add(this.cbactpro);
            this.Controls.Add(this.txtmail);
            this.Controls.Add(this.txtdirpro);
            this.Controls.Add(this.txttelpro);
            this.Controls.Add(this.txtdniprof);
            this.Controls.Add(this.txtnomape);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AgregarProf";
            this.Text = "AgregarProf";
            this.Load += new System.EventHandler(this.AgregarProf_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbactpro;
        private System.Windows.Forms.TextBox txtmail;
        private System.Windows.Forms.TextBox txtdirpro;
        private System.Windows.Forms.TextBox txttelpro;
        private System.Windows.Forms.TextBox txtdniprof;
        private System.Windows.Forms.TextBox txtnomape;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpfn;
        private System.Windows.Forms.TextBox txtnro;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbloc;
        private System.Windows.Forms.ComboBox cbprov;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btagregarpro;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cbdia;
        private System.Windows.Forms.ComboBox cbhorp;
        private System.Windows.Forms.Label label12;
    }
}