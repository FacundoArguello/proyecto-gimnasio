﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//
using ConectorBD;
using System.Configuration;
using System.Text.RegularExpressions;

namespace Gymtesis
{
    public partial class AgregarProf : Form
    {
        DatosMySQL objDatos;
        DataTable dt;
        string cadCn;
        int valor;
        public AgregarProf()
        {
            InitializeComponent();
        }
        private void AgregarProf_Load(object sender, EventArgs e)
        {
            cadCn = ConfigurationManager.ConnectionStrings["MYSQL"].ConnectionString;           
            comboprov();
            comboactprof();
            traerDias();

        }
        private void cbprov_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRowView dr = (DataRowView)cbprov.SelectedItem;
            int id = Convert.ToInt32(dr[0]);

            string lpSQL = ApTables.locxprov(id.ToString());
            dt = new DataTable();
            objDatos=new DatosMySQL();
            dt = objDatos.traerDataTable(lpSQL, cadCn);

            if (dt != null)
            {
                cbloc.ValueMember = "idloc";
                cbloc.DisplayMember = "nomloc";
                cbloc.DataSource = dt;
            }
        }

        private void comboactprof()
        {
            string actSQL = ApTables.datosAct();          
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(actSQL, cadCn);

            cbactpro.ValueMember = "idactividad";
            cbactpro.DisplayMember = "nomact";
            cbactpro.DataSource = dt;
            
        }

        private void comboprov()
        {
            string provSQL = ApTables.datosProv();
            dt = new DataTable();
            objDatos = new DatosMySQL();
           
            dt = objDatos.traerDataTable(provSQL, cadCn);

            cbprov.ValueMember = "idprov";
            cbprov.DisplayMember = "nomprov";
            cbprov.DataSource = dt;
        }
        private void traerDias()
        {
            string DiSQL = ApTables.obtenerDias();
            objDatos = new DatosMySQL();
            dt = new DataTable();

            dt = objDatos.traerDataTable(DiSQL, cadCn);

            cbdia.ValueMember = "iddia";
            cbdia.DisplayMember = "Dias";
            cbdia.DataSource = dt;
        }
        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void cbloc_SelectedIndexChanged(object sender, EventArgs e)
        {

        }  
        private void agregarBDpro()
        {            
            string fechanac = (Convert.ToDateTime(dtpfn.Text)).ToString("dd-MM-yyyy");
            string agSQLp = "insert into gimnasio.profesionales (nomape, dni, tel, fenac, direccion, nro, mail, idact, iddia, idhorario, loc)" +
                "values ('" + txtnomape.Text + "'," + txtdniprof.Text + "," + txttelpro.Text + ",'" + fechanac +
                "','" + txtdirpro.Text + "'," + txtnro.Text + ",'" + txtmail.Text + "'," + Convert.ToInt32(cbactpro.SelectedValue) + ",'" +Convert.ToInt32(cbdia.SelectedValue) + "','" + Convert.ToInt32(cbhorp.SelectedValue) + "'," + Convert.ToInt32(cbloc.SelectedValue) + ")";
            objDatos = new DatosMySQL();
            valor = objDatos.ejecutarSQL(agSQLp, cadCn);
                
        }
        public static bool validaremail(string email)
        {
            string expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

            if(Regex.IsMatch(email, expresion))
            {
                if(Regex.Replace(email, expresion, String.Empty).Length==0)
                { return true; }
                else
                { return false; }
            }
            else
            { return false; }
        }
        private void btagregarpro_Click(object sender, EventArgs e)
        {
            if (Validar() == true)
            {
                agregarBDpro();
                this.Close();
            }
        }
        private bool Validar()
        {
            if(txtnomape.Text== "")
            {
                MessageBox.Show("Debe ingresar un nombre y apellido");
                txtnomape.Focus();
                return false;
            }
            if(txtdniprof.Text == "")
            {
                MessageBox.Show("Debe ingresar el DNI");
                txtdniprof.Focus();
                return false;
            }
            if(txttelpro.Text=="")
            {
                MessageBox.Show("Debe ingresar un telefono");
                txttelpro.Focus();
                return false;
            }
            if (txtdirpro.Text == "")
            {
                MessageBox.Show("Debe ingresar una direcciòn");
                txtdirpro.Focus();
                return false;
            }
            if (txtnro.Text == "")
            {
                MessageBox.Show("Debe ingresar el numero de la calle");
                txtnro.Focus();
                return false;
            }
            if (txtmail.Text == "")
            {
                MessageBox.Show("Debe ingresar su mail");
                txtmail.Focus();
                return false;
            }
            return true;
        }
    

        private void cbactpro_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void txtmail_Leave(object sender, EventArgs e)
        {
            validaremail(txtmail.Text);
        }

        private void txthor_Leave(object sender, EventArgs e)
        {
            validaremail(cbhorp.Text);
        }

        private void txtdniprof_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txttelpro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void comboHora()
        {
            string hSQL = "select * from gimnasio.horariosprof  where idhorp not in (select idhorario from profesionales where iddia =" + cbdia.SelectedValue + ")";
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(hSQL, cadCn);

            if (dt != null)
            {
                cbhorp.ValueMember = "idhorp";
                cbhorp.DisplayMember = "Horarios";
                cbhorp.DataSource = dt;
            }
        }

    private void cbhorp_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cbdia_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboHora();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void buscardniigualp()
        {
            if (txtdniprof.Text == "") return;
            string dniSQL = "select dni from profesionales where dni =" + txtdniprof.Text;
            dt = new DataTable();
            objDatos = new DatosMySQL();
            dt = objDatos.traerDataTable(dniSQL, cadCn);

            if (dt == null) return;

            string dniigp = dt.Rows[0]["dni"].ToString();
            if (dniigp == txtdniprof.Text)
            {
                try
                {
                    MessageBox.Show("Numero de documento ingresado anteriormente en el sistema");
                    txtdniprof.Text = "";
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void txtdniprof_Validating(object sender, CancelEventArgs e)
        {
            buscardniigualp();
        }

        private void txtnro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
