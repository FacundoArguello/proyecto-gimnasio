﻿namespace Gymtesis
{
    partial class Agregarcliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Agregarcliente));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtnomc = new System.Windows.Forms.TextBox();
            this.txtapellc = new System.Windows.Forms.TextBox();
            this.txtdnic = new System.Windows.Forms.TextBox();
            this.txttelc = new System.Windows.Forms.TextBox();
            this.txtedadc = new System.Windows.Forms.TextBox();
            this.cbact = new System.Windows.Forms.ComboBox();
            this.btagregar = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtimprimir = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.cbprofesor = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 35);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(235, 36);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Apellido";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(24, 77);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "DNI";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(24, 127);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Telefono";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(24, 177);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Edad";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(22, 223);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "Actividad";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(24, 329);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 16);
            this.label7.TabIndex = 6;
            this.label7.Text = "Inscripcion";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(24, 379);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 16);
            this.label8.TabIndex = 7;
            this.label8.Text = "Expiracion";
            // 
            // txtnomc
            // 
            this.txtnomc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtnomc.Location = new System.Drawing.Point(96, 35);
            this.txtnomc.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtnomc.Name = "txtnomc";
            this.txtnomc.Size = new System.Drawing.Size(130, 20);
            this.txtnomc.TabIndex = 8;
            // 
            // txtapellc
            // 
            this.txtapellc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtapellc.Location = new System.Drawing.Point(301, 35);
            this.txtapellc.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtapellc.Name = "txtapellc";
            this.txtapellc.Size = new System.Drawing.Size(130, 20);
            this.txtapellc.TabIndex = 9;
            // 
            // txtdnic
            // 
            this.txtdnic.Location = new System.Drawing.Point(96, 77);
            this.txtdnic.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtdnic.MaxLength = 8;
            this.txtdnic.Name = "txtdnic";
            this.txtdnic.Size = new System.Drawing.Size(132, 20);
            this.txtdnic.TabIndex = 10;
            this.txtdnic.TextChanged += new System.EventHandler(this.txtdnic_TextChanged);
            this.txtdnic.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtdnic_KeyPress);
            this.txtdnic.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtdnic_KeyUp);
            this.txtdnic.Validating += new System.ComponentModel.CancelEventHandler(this.txtdnic_Validating);
            // 
            // txttelc
            // 
            this.txttelc.Location = new System.Drawing.Point(96, 127);
            this.txttelc.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txttelc.MaxLength = 15;
            this.txttelc.Name = "txttelc";
            this.txttelc.Size = new System.Drawing.Size(132, 20);
            this.txttelc.TabIndex = 11;
            this.txttelc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txttelc_KeyPress);
            // 
            // txtedadc
            // 
            this.txtedadc.Location = new System.Drawing.Point(96, 177);
            this.txtedadc.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtedadc.MaxLength = 2;
            this.txtedadc.Name = "txtedadc";
            this.txtedadc.Size = new System.Drawing.Size(132, 20);
            this.txtedadc.TabIndex = 12;
            this.txtedadc.TextChanged += new System.EventHandler(this.txtedadc_TextChanged);
            this.txtedadc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtedadc_KeyPress);
            // 
            // cbact
            // 
            this.cbact.FormattingEnabled = true;
            this.cbact.Location = new System.Drawing.Point(96, 220);
            this.cbact.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbact.Name = "cbact";
            this.cbact.Size = new System.Drawing.Size(132, 21);
            this.cbact.TabIndex = 16;
            this.cbact.SelectedIndexChanged += new System.EventHandler(this.cbact_SelectedIndexChanged);
            // 
            // btagregar
            // 
            this.btagregar.FlatAppearance.BorderSize = 0;
            this.btagregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btagregar.Image = ((System.Drawing.Image)(resources.GetObject("btagregar.Image")));
            this.btagregar.Location = new System.Drawing.Point(24, 35);
            this.btagregar.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btagregar.Name = "btagregar";
            this.btagregar.Size = new System.Drawing.Size(139, 70);
            this.btagregar.TabIndex = 17;
            this.btagregar.Text = "Agregar";
            this.btagregar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btagregar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btagregar.UseVisualStyleBackColor = true;
            this.btagregar.Click += new System.EventHandler(this.btagregar_Click);
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(24, 193);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(139, 70);
            this.button2.TabIndex = 18;
            this.button2.Text = "Cancelar";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(121)))), ((int)(((byte)(78)))));
            this.panel1.Controls.Add(this.txtimprimir);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.btagregar);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.ForeColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(439, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(168, 436);
            this.panel1.TabIndex = 19;
            // 
            // txtimprimir
            // 
            this.txtimprimir.FlatAppearance.BorderSize = 0;
            this.txtimprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.txtimprimir.Image = ((System.Drawing.Image)(resources.GetObject("txtimprimir.Image")));
            this.txtimprimir.Location = new System.Drawing.Point(24, 111);
            this.txtimprimir.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtimprimir.Name = "txtimprimir";
            this.txtimprimir.Size = new System.Drawing.Size(139, 70);
            this.txtimprimir.TabIndex = 20;
            this.txtimprimir.Text = "Imprimir";
            this.txtimprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txtimprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.txtimprimir.UseVisualStyleBackColor = true;
            this.txtimprimir.Click += new System.EventHandler(this.txtimprimir_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Location = new System.Drawing.Point(10, 279);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(153, 153);
            this.panel2.TabIndex = 19;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(121, 329);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(132, 20);
            this.dateTimePicker1.TabIndex = 20;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(121, 379);
            this.dateTimePicker2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(132, 20);
            this.dateTimePicker2.TabIndex = 21;
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // cbprofesor
            // 
            this.cbprofesor.FormattingEnabled = true;
            this.cbprofesor.Location = new System.Drawing.Point(311, 220);
            this.cbprofesor.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbprofesor.Name = "cbprofesor";
            this.cbprofesor.Size = new System.Drawing.Size(120, 21);
            this.cbprofesor.TabIndex = 22;
            this.cbprofesor.SelectedIndexChanged += new System.EventHandler(this.cbprofesor_SelectedIndexChanged);
            this.cbprofesor.SelectionChangeCommitted += new System.EventHandler(this.cbprofesor_SelectionChangeCommitted);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(150, 279);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 16);
            this.label9.TabIndex = 23;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(313, 279);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 16);
            this.label10.TabIndex = 24;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(244, 225);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 16);
            this.label11.TabIndex = 25;
            this.label11.Text = "Profesor";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(257, 279);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 16);
            this.label13.TabIndex = 49;
            this.label13.Text = "Horario:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(118, 279);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 16);
            this.label12.TabIndex = 48;
            this.label12.Text = "Dia:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(72, 279);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(0, 16);
            this.label14.TabIndex = 50;
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(22, 279);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 16);
            this.label15.TabIndex = 51;
            this.label15.Text = "Precio:";
            // 
            // Agregarcliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(607, 436);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cbprofesor);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cbact);
            this.Controls.Add(this.txtedadc);
            this.Controls.Add(this.txttelc);
            this.Controls.Add(this.txtdnic);
            this.Controls.Add(this.txtapellc);
            this.Controls.Add(this.txtnomc);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Agregarcliente";
            this.Text = "Agregarcliente";
            this.Load += new System.EventHandler(this.Agregarcliente_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtnomc;
        private System.Windows.Forms.TextBox txtapellc;
        private System.Windows.Forms.TextBox txtdnic;
        private System.Windows.Forms.TextBox txttelc;
        private System.Windows.Forms.TextBox txtedadc;
        private System.Windows.Forms.ComboBox cbact;
        private System.Windows.Forms.Button btagregar;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Button txtimprimir;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cbprofesor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
    }
}