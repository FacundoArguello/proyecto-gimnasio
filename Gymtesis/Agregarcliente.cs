﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//
using ConectorBD;
using System.Configuration;
using Gma.QrCodeNet.Encoding.Windows.Render;
using Gma.QrCodeNet.Encoding;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing.Printing;


namespace Gymtesis
{
    public partial class Agregarcliente : Form
    {
        DatosMySQL objDatos;
        DataTable dt;
        int valor;
        string cadCn;
        public Agregarcliente()
        {
            InitializeComponent();
        }
        private void Agregarcliente_Load(object sender, EventArgs e)
        {
            cadCn = ConfigurationManager.ConnectionStrings["MYSQL"].ConnectionString;
            obteneract();
        }
       
        private void obteneract()
        {
            string aSQL = ApTables.comboact();
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(aSQL, cadCn);
            if (dt == null) return;
            cbact.ValueMember = "idactivida";
            cbact.DisplayMember = "nomact";           
            cbact.DataSource = dt;
            
        }
        private void obprofesor()
        {
            DataRowView dr = (DataRowView)cbact.SelectedItem;
            int id = Convert.ToInt32(dr[0]);
            string prfSQL = "select * from gimnasio.profesionales where idact = " + id.ToString() + "";
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(prfSQL, cadCn);

            if (dt == null) return;
            cbprofesor.ValueMember = "idprof";
            cbprofesor.DisplayMember = "nomape";
            cbprofesor.DataSource = dt;
        } 
        private void llenarlabels()
        {
            
            string labSQL = "select d.Dias, h.Horarios from gimnasio.profesionales p inner join diasprof d on p.iddia = d.iddia inner join horariosprof h on p.idhorario = h.idhorp where p.idprof = " + cbprofesor.SelectedValue ;
            dt = new DataTable();
            objDatos = new DatosMySQL();
            dt = objDatos.traerDataTable(labSQL, cadCn);
            if (dt == null) return;
            label9.Text = dt.Rows[0]["Dias"].ToString();
            label10.Text = dt.Rows[0]["Horarios"].ToString();
        }
        private void llenarpreciolabel()
        {
            DataRowView dr = (DataRowView)cbact.SelectedValue;

            string idpre = dr[0].ToString();
            string preSQL = "select precio from gimnasio.actividad where idactividad=" + idpre ;
            dt = new DataTable();
            objDatos = new DatosMySQL();
            dt = objDatos.traerDataTable(preSQL, cadCn);
            if (dt == null) return;
            label14.Text = dt.Rows[0]["precio"].ToString();
        }
        private void agregarbd()
        {
            DataRowView dr = (DataRowView)cbact.SelectedValue;

            string id = dr[0].ToString();
            string fechaUno = (Convert.ToDateTime(dateTimePicker1.Text)).ToString("yyyy-MM-dd");
            string fechaDos = (Convert.ToDateTime(dateTimePicker2.Text)).ToString("yyyy-MM-dd");

            string agrSQL = "insert into gimnasio.cliente(nomcli, apellcli, dni, tel, edad, idact, idprof, fepago, feexp)" +
               " values('" + txtnomc.Text + "','" + txtapellc.Text + "'," + txtdnic.Text + "," +
               txttelc.Text + "," + txtedadc.Text + "," + id + "," + Convert.ToInt32(cbprofesor.SelectedValue) +
               ",'" + fechaUno + "','" + fechaDos + "')";

            objDatos = new DatosMySQL();

            valor = objDatos.ejecutarSQL(agrSQL, cadCn);
        }
        private void Capacidaddeactividades()
        {
            DataRowView cba = (DataRowView)cbact.SelectedValue;
            string idac = cba[0].ToString();     

            string capSQL = "select a.capacidad, count(*) as cantidad " +
                "from cliente c inner join actividad a on c.idact = a.idactividad" +
                " where c.idact =" + idac + " and c.idprof =" + Convert.ToInt32(cbprofesor.SelectedValue);
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(capSQL, cadCn);
            if (dt == null) return;
            int cap = Convert.ToInt32(dt.Rows[0]["capacidad"]);
            int cant = Convert.ToInt32(dt.Rows[0]["cantidad"]);
            if (cap > cant)
            {
                MessageBox.Show("Hay capacidad");
                agregarbd();
                this.Close();
            }
            else
            {
                MessageBox.Show("No hay cupo");
            }
        }  
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btagregar_Click(object sender, EventArgs e)
        {
            if (validar() == true)
            {
                Capacidaddeactividades();
                //agregarbd();
                //this.Close();
            }
        }

        private void cbact_SelectedIndexChanged(object sender, EventArgs e)
        {
            obprofesor();
            llenarpreciolabel();
            
        }
        private bool validar()
        {
            if(txtnomc.Text== "")
            {
                MessageBox.Show("Debe ingresar nombre");
                txtnomc.Focus();
                return false;
            }
            if(txtapellc.Text == "")
            {
                MessageBox.Show("Debe ingresar apellido");
                txtapellc.Focus();
                return false;
            }
            if(txtdnic.Text=="")
            {
                MessageBox.Show("Debe ingresar el DNI");
                txtdnic.Focus();
                return false;
            }
            if(txttelc.Text=="")
            {
                MessageBox.Show("Debe ingresar el DNI");
                txttelc.Focus();
                return false;
            }
            if(txtedadc.Text=="")
            {
                MessageBox.Show("Debe ingresar la edad");
                txtedadc.Focus();
                return false;
            }
            return true;
        }
        private void txtdnic_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if(Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
           
        }

        private void txttelc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))//Para obligar a que sólo se introduzcan números
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar))//permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;//el resto de teclas pulsadas se desactivan
            }
        }

        private void txtedadc_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void txtedadc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))//Para obligar a que sólo se introduzcan números
            {
                e.Handled = false;
            }
            else
               if (Char.IsControl(e.KeyChar))//permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;//el resto de teclas pulsadas se desactivan
            }
        }

        private void txtimprimir_Click(object sender, EventArgs e)
        {
            if (valImpresora() == true)
            {
                DialogResult ok = MessageBox.Show("Desea usted imprimir?", "Imprimir", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (ok == System.Windows.Forms.DialogResult.Yes)
                {
                    imprimircarnet();
                }
            }     
        }
        private bool valImpresora()
        {
            if(txtnomc.Text=="")
            {
                MessageBox.Show("Debe ingresar Nombre");
                txtnomc.Focus();
                return false;
            }
            if (txtapellc.Text == "")
            {
                MessageBox.Show("Debe ingresar apellido");
                txtapellc.Focus();
                return false;
            }
            if (txtdnic.Text == "")
            {
                MessageBox.Show("Debe ingresar el DNI");
                txtdnic.Focus();
                return false;
            }
            if (txttelc.Text == "")
            {
                MessageBox.Show("Debe ingresar el telefono");
                txtdnic.Focus();
                return false;
            }
            return true;
        }
        private void txtdnic_TextChanged(object sender, EventArgs e)
        {
            //Genera el codigo QR atravez del DNI y se lo muestra en un panel
            QrEncoder qr = new QrEncoder(ErrorCorrectionLevel.H);
            QrCode qrcode = new QrCode();
            qr.TryEncode(txtdnic.Text, out qrcode);
            GraphicsRenderer gra = new GraphicsRenderer(new FixedCodeSize(300, QuietZoneModules.Zero), Brushes.Black, Brushes.White);
            MemoryStream ms = new MemoryStream();
            gra.WriteToStream(qrcode.Matrix, ImageFormat.Png, ms);
            var imagetemporal = new Bitmap(ms);
            var image = new Bitmap(imagetemporal, new Size(new Point(150, 150)));
            panel2.BackgroundImage = image;
            
        }
        private string Nombre;
        private string Apellido;//Variables para imprimir 
        private string DNI;
        private string telefono;
        private Font fuente = new Font("Arial", 25);

        public void imprimircarnet()
        {
            PrintDocument cod = new PrintDocument();
            cod.PrintPage += new PrintPageEventHandler(datosocios);
            PrintDialog prd = new PrintDialog();
            prd.Document = cod;
            DialogResult result = prd.ShowDialog();

            if (result == DialogResult.OK)
            {
                cod.Print();
            }

        }
        private void datosocios(object obj, PrintPageEventArgs ev)
        {
            float pos_x = 10;
            float pos_y = 25;
            Nombre = txtnomc.Text;
            Apellido = txtapellc.Text;
            telefono = txttelc.Text;
            DNI = txtdnic.Text;
            //Tamaños y posiciones de los nombres que se van a imprimir
            ev.Graphics.DrawString("Nombre:", fuente, Brushes.Black, pos_x, pos_y, new StringFormat());
            ev.Graphics.DrawString("Apellido:", fuente, Brushes.Black, pos_x, pos_y + 50, new StringFormat());
            ev.Graphics.DrawString("DNI:", fuente, Brushes.Black, pos_x, pos_y + 100, new StringFormat());
            ev.Graphics.DrawString("Telefono:", fuente, Brushes.Black, pos_x, pos_y + 150, new StringFormat());

            ev.Graphics.DrawString(Nombre, fuente, Brushes.Black, pos_x + 150, pos_y, new StringFormat());
            ev.Graphics.DrawString(Apellido, fuente, Brushes.Black, pos_x + 150, pos_y + 50, new StringFormat());
            ev.Graphics.DrawString(DNI, fuente, Brushes.Black, pos_x + 90, pos_y + 100, new StringFormat());
            ev.Graphics.DrawString(telefono, fuente, Brushes.Black, pos_x + 160, pos_y + 150, new StringFormat());
            ev.Graphics.DrawImage(panel2.BackgroundImage, 380, 25, 180, 180);

        }

        private void cbprofesor_SelectedIndexChanged(object sender, EventArgs e)
        {
            llenarlabels();
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            var dtp = sender as DateTimePicker;

            if (dtp.Value.DayOfWeek == DayOfWeek.Saturday || dtp.Value.DayOfWeek == DayOfWeek.Sunday)
            {
                MessageBox.Show("No es posible seleccionar el día sábado o domingo");
                //Selecciona el último viernes
                dtp.Value = dtp.Value.AddDays(dtp.Value.DayOfWeek == DayOfWeek.Sunday ? -2 : -1);
                dtp.Focus();
            }
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            var dtp1 = sender as DateTimePicker;

            if (dtp1.Value.DayOfWeek == DayOfWeek.Saturday || dtp1.Value.DayOfWeek == DayOfWeek.Sunday)
            {
                MessageBox.Show("No es posible seleccionar el día sábado o domingo");
                //Selecciona el último viernes
                dtp1.Value = dtp1.Value.AddDays(dtp1.Value.DayOfWeek == DayOfWeek.Sunday ? -2 : -1);
                dtp1.Focus();
            }
        }

        private void cbprofesor_SelectionChangeCommitted(object sender, EventArgs e)
        {
            DataRowView cbac = (DataRowView)cbact.SelectedValue;
            string idact = cbac[0].ToString();

            string msgcapSQL = "select a.capacidad, count(*) as cantidad " +
                "from cliente c inner join actividad a on c.idact = a.idactividad" +
                " where c.idact =" + idact + " and c.idprof =" + Convert.ToInt32(cbprofesor.SelectedValue);
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(msgcapSQL, cadCn);
            if (dt == null) return;
            int cap = Convert.ToInt32(dt.Rows[0]["capacidad"]);
            int cant = Convert.ToInt32(dt.Rows[0]["cantidad"]);
            if (cap > cant)
            {
                MessageBox.Show("Hay capacidad");                              
            }
            else
            {
                MessageBox.Show("No hay cupo");
            }
        }
        private void buscardniigual()
        {
            if (txtdnic.Text == "") return;
            string dniSQL = "select dni from cliente where dni =" + txtdnic.Text;
            dt = new DataTable();
            objDatos = new DatosMySQL();
            dt = objDatos.traerDataTable(dniSQL, cadCn);
            
            if (dt == null) return;
            
            string dniig = dt.Rows[0]["dni"].ToString();
            if(dniig == txtdnic.Text)
            {
                try
                {
                    MessageBox.Show("Numero de documento ingresado anteriormente en el sistema");
                    txtdnic.Text = "";
                }
                catch(SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void txtdnic_KeyUp(object sender, KeyEventArgs e)
        {
            
        }

        private void txtdnic_Validating(object sender, CancelEventArgs e)
        {
            buscardniigual();
        }
    }
}
