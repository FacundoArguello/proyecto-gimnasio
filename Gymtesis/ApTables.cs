﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gymtesis
{
    public class ApTables
    {
        #region CLIENTES     
        public static string obclientes()
        {
            return "select c.idcliente 'ID', c.nomcli 'Nombre', c.apellcli 'Apellido' , c.dni 'DNI', c.tel 'Telefono', c.edad 'Edad', a.nomact'Actividad', p.nomape 'Profesor',c.fepago'Fecha de pago', c.feexp'Fecha de expiracion'  from gimnasio.cliente c inner join actividad a on c.idact = a.idactividad  inner join profesionales p on c.idprof= p.idprof where c.idcliente order by c.apellcli asc, c.nomcli asc";
        }
        public static string comboact()
        {
            return "select * from gimnasio.actividad";
        }
        public static string eliminarcliente(string dn)
        {
            return "delete from gimnasio.cliente where dni=" + dn;
        }
        public static string actualizarclientes(string ac)
        {
            return "select c.idcliente 'ID', c.nomcli 'Nombre', c.apellcli 'Apellido' , c.dni 'DNI', c.tel 'Telefono', c.edad 'Edad', a.nomact'Actividad', p.nomape 'Profesor',c.fepago'Fecha de pago', c.feexp'Fecha de expiracion'  from gimnasio.cliente c inner join actividad a on c.idact = a.idactividad  inner join profesionales p on c.idprof= p.idprof where c.idcliente " + ac + "order by c.apellcli asc, c.nomcli asc";
        }
        #endregion
        #region PROFESIONALES
        public static string actfesionales(string pro)
        {
            return "select p.idprof 'ID', p.nomape 'Nombre y Apellido', p.dni 'DNI', p.tel 'Telefono', p.fenac 'Fecha de nacimiento', p.direccion 'Direccion', p.nro 'Numero', p.mail 'Mail', a.nomact'Actividad', d.Dias, h.Horarios,l.nomloc 'Localidad' from (gimnasio.profesionales p inner join actividad a on p.idact = a.idactividad) inner join localidades l on p.loc = l.idloc inner join diasprof d on p.iddia= d.iddia inner join horariosprof h on p.idhorario = h.idhorp where p.idprof" + pro;
        }
        public static string eliminarprofesionales(string epr)
        {
            return "delete from gimnasio.profesionales where dni=" + epr;
        }
        public static string locxprov(string lp)
        {
            return "select * from gimnasio.localidades where idprov =" + lp;
        }
        public static string datosProv()
        {
            return "select * from gimnasio.provincias";
        }
        public static string datosLoc()
        {
            return "select * from gimnasio.localidades";
        }
        public static string datosAct()
        {
            return "select * from gimnasio.actividad";
        }
        public static string obtenerDias()
        {
            return "select * from gimnasio.diasprof";
        }
        public static string obtenerhoras()
        {
            return "select * from gimnasio.horariosprof";
        }
        public static string comboProfformmodclientes()
        {
            return "select * from gimnasio.profesionales";
        }

        #endregion
        #region ACTIVIDADES
        public static string BDactividades()
        {
            return "select idactividad 'ID', nomact 'Nombre', precio 'Precio', capacidad 'Capacidad' from gimnasio.actividad order by nomact asc";
        }
        public static string BDcomboprof()
        {
            return "select * from gimnasio.profesionales";
        }
        public static string BDeliminaract(string idactividad)
        {
            return "delete  from gimnasio.actividad where idactividad = " + idactividad;
        }
        #endregion
        #region NUTRICIONISTA
        public static string BDturnero()
        {
            return "select t.dniclien 'DNI', t.nomape 'Nombre y Apellido', t.fecha 'Fecha', h.rango 'Hora' from gimnasio.turnos t inner join horario h  on  t.hora = h.idhor order by t.fecha asc, h.rango asc";
        }
        public static string BDelimturnos(string etur)
        {
            return "delete  from gimnasio.turnos where dniclien =" + etur;
        }
        public static string BDHORA()
        {
            return "select * from gimnasio.horario";
        }
        public static string BDHORAEXP()
        {
            return "select * from gimnasio.horario where idhor not in (select hora from turnos)";
        }
        public static string BDBORRFECH()
        {
            return "delete from turnos where fecha < CURDATE()";
        }
        #endregion
        #region INGRESO
       public static string expiracion()
        {
            return "select c.idcliente 'ID', c.nomcli 'Nombre', c.apellcli 'Apellido' , c.dni 'DNI', c.tel 'Telefono', c.edad 'Edad', a.nomact'Actividad', p.nomape 'Profesor',c.fepago'Fecha de pago', c.feexp'Fecha de expiracion', datediff(c.feexp, curdate()) as 'Dias de expiracion'  from gimnasio.cliente c inner join actividad a on c.idact = a.idactividad  inner join profesionales p on c.idprof= p.idprof order by c.apellcli asc, c.nomcli asc ";
        }

        #endregion
        #region CONSULTAS
        public static string PDFCLIEXP()
        {
            return "select c.nomcli, c.apellcli, c.dni, a.nomact, p.nomape, c.feexp  from cliente c inner join actividad a on c.idact= a.idactividad inner join profesionales p on c.idprof = p.idprof where c.feexp < curdate()";
        }
        public static string PDFCLIACT()
        {
            return "select c.nomcli, c.apellcli, c.dni, a.nomact, p.nomape, c.feexp  from cliente c inner join actividad a on c.idact= a.idactividad inner join profesionales p on c.idprof = p.idprof where c.feexp > curdate()";
        }
        public static string ComboACT()
        {
            return "select * from actividad";
        }
        public static string PDFNUTRI()
        {
            return "Select t.dniclien, t.nomape, t.fecha, h.rango from turnos t inner join horario h on t.hora= h.idhor where t.fecha = curdate()";
        }
        #endregion
    }
}
