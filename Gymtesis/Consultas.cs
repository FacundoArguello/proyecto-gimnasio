﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//
using iTextSharp.text;
using iTextSharp.text.pdf;
using ConectorBD;
using System.Configuration;

 

namespace Gymtesis
{
    public partial class Consultas : Form
    {
        DataTable dt;
        DatosMySQL objDatos;
        string cadCn;
        public Consultas()
        {
            InitializeComponent();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void Consultas_Load(object sender, EventArgs e)
        {
            cadCn = ConfigurationManager.ConnectionStrings["MYSQL"].ConnectionString;
            groupBox1.Enabled = false;
            obactxcon();
        }

        private void obactxcon()
        {
            string conactSQL = ApTables.ComboACT();
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(conactSQL, cadCn);

            if (dt == null) return;
            comboBox1.ValueMember = "idactividad";
            comboBox1.DisplayMember = "nomact";
            comboBox1.DataSource = dt;
        }
        private void obprofxact()
        {
            DataRowView dr = (DataRowView)comboBox1.SelectedItem;
            int id = Convert.ToInt32(dr[0]);
            string prfSQL = "select * from gimnasio.profesionales where idact = " + id.ToString() + "";
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(prfSQL, cadCn);

            if (dt == null) return;
            comboBox2.ValueMember = "idprof";
            comboBox2.DisplayMember = "nomape";
            comboBox2.DataSource = dt;
        }

        private void PDFexp()
        {
            try
            {
                string pdfexpSQL = ApTables.PDFCLIEXP();
                dt = new DataTable();
                objDatos = new DatosMySQL();

                dt = objDatos.traerDataTable(pdfexpSQL, cadCn);

                if (dt == null)
                {
                    MessageBox.Show("No hay clientes expirados");
                }
                else
                {
                    Document doc = new Document(PageSize.A4);
                    PdfWriter writer = PdfWriter.GetInstance(doc, new System.IO.FileStream("Alumnos expirados.pdf", System.IO.FileMode.Create));
                    doc.AddTitle("Clientes expirados");

                    doc.AddCreator("Facundo Arguello");

                    doc.Open();
                    //doc.Add(new Paragraph("Clientes expirados"));
                    //doc.Add(Chunk.NEWLINE);
                    PdfPTable tabla = new PdfPTable(6);
                    // ancho real de la tabla en puntos
                    tabla.TotalWidth = 550f;
                    // arregla el ancho absoluto de la tabla
                    tabla.LockedWidth = true;

                    // anchos de columna relativos en proporciones - 1/3 y 2/3
                    float[] widths = new float[] { 9f, 10f, 7f, 10f, 10f, 8f };
                    tabla.SetWidths(widths);
                    tabla.HorizontalAlignment = 0;
                    //leave a gap before and after the table
                    tabla.SpacingBefore = 20f;
                    tabla.SpacingAfter = 40f;

                    PdfPCell cell = new PdfPCell(new Phrase("Clientes expirados"));
                    cell.Colspan = 6;
                    cell.Border = 0;
                    cell.HorizontalAlignment = 10;
                    tabla.AddCell(cell);


                    BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
                    iTextSharp.text.Font times =
                      new iTextSharp.text.Font(bfTimes, 14, iTextSharp.text.Font.NORMAL);
                    foreach (DataRow dr in dt.Rows)
                    {
                        tabla.AddCell(new Phrase(dr["nomcli"].ToString(), times));
                        tabla.AddCell(new Phrase(dr["apellcli"].ToString(), times));
                        tabla.AddCell(new Phrase(dr["dni"].ToString(), times));
                        tabla.AddCell(new Phrase(dr["nomact"].ToString(), times));
                        tabla.AddCell(new Phrase(dr["nomape"].ToString(), times));
                        tabla.AddCell(new Phrase(dr["feexp"].ToString(), times));
                    }

                    doc.Add(tabla);
                    doc.Close();
                    writer.Close();
                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo.FileName = "Alumnos expirados.pdf";
                    proc.Start();
                    proc.Close();

                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void PDFACT()
        {
            try
            {
                string pdfexpSQL = ApTables.PDFCLIACT();
                dt = new DataTable();
                objDatos = new DatosMySQL();

                dt = objDatos.traerDataTable(pdfexpSQL, cadCn);

                if (dt == null)
                {
                    MessageBox.Show("No hay alumnos activos");
                }
                else
                {
                    Document doc = new Document(PageSize.A4);
                    PdfWriter writer = PdfWriter.GetInstance(doc, new System.IO.FileStream("Alumnos activos.pdf", System.IO.FileMode.Create));
                    doc.AddTitle("Clientes activdos");

                    doc.AddCreator("Facundo Arguello");

                    doc.Open();
                    //doc.Add(new Paragraph("Clientes expirados"));
                    //doc.Add(Chunk.NEWLINE);
                    PdfPTable tabla = new PdfPTable(6);
                    // ancho real de la tabla en puntos
                    tabla.TotalWidth = 550f;
                    // arregla el ancho absoluto de la tabla
                    tabla.LockedWidth = true;

                    // anchos de columna relativos en proporciones - 1/3 y 2/3
                    float[] widths = new float[] { 9f, 10f, 7f, 10f, 10f, 8f };
                    tabla.SetWidths(widths);
                    tabla.HorizontalAlignment = 0;
                    //leave a gap before and after the table
                    tabla.SpacingBefore = 20f;
                    tabla.SpacingAfter = 40f;

                    PdfPCell cell = new PdfPCell(new Phrase("Alumnos activos"));
                    cell.Colspan = 6;
                    cell.Border = 0;
                    cell.HorizontalAlignment = 10;
                    tabla.AddCell(cell);

                    BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
                    iTextSharp.text.Font times =
                      new iTextSharp.text.Font(bfTimes, 14, iTextSharp.text.Font.NORMAL);
                    foreach (DataRow dr in dt.Rows)
                    {
                        tabla.AddCell(new Phrase(dr["nomcli"].ToString(), times));
                        tabla.AddCell(new Phrase(dr["apellcli"].ToString(), times));
                        tabla.AddCell(new Phrase(dr["dni"].ToString(), times));
                        tabla.AddCell(new Phrase(dr["nomact"].ToString(), times));
                        tabla.AddCell(new Phrase(dr["nomape"].ToString(), times));
                        tabla.AddCell(new Phrase(dr["feexp"].ToString(), times));
                    }

                    doc.Add(tabla);
                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo.FileName = "Alumnos activos.pdf";
                    proc.Start();
                    proc.Close();
                    doc.Close();
                    writer.Close();
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void PDFACTxCLI()
        {
            try
            {
                string actxclSQL = "select c.nomcli, c.apellcli, c.dni, c.edad, a.nomact, p.nomape from cliente c inner join actividad a on c.idact= a.idactividad inner join profesionales p on c.idprof = p.idprof where c.idact= " + Convert.ToInt32(comboBox1.SelectedValue) + " and c.idprof= " + Convert.ToInt32(comboBox2.SelectedValue);
                dt = new DataTable();
                objDatos = new DatosMySQL();

                dt = objDatos.traerDataTable(actxclSQL, cadCn);
                if (dt == null)
                {
                    MessageBox.Show("No hay alumnos en esta actividad");
                }
                else
                {
                    Document doc = new Document(PageSize.A4);
                    PdfWriter writer = PdfWriter.GetInstance(doc, new System.IO.FileStream("Clientes por actividad.pdf", System.IO.FileMode.Create));

                    doc.AddTitle("Clientes por actividad");
                    doc.AddAuthor("Facundo Arguello");

                    doc.Open();

                    PdfPTable tabla = new PdfPTable(6);
                    tabla.TotalWidth = 550;
                    tabla.LockedWidth = true;

                    float[] widths = new float[] { 8f, 8f, 8f, 5f, 10f, 10f };

                    tabla.SetWidths(widths);
                    tabla.HorizontalAlignment = 0;

                    tabla.SpacingBefore = 30f;
                    tabla.SpacingAfter = 60f;

                    PdfPCell cells = new PdfPCell(new Phrase("Alumnos por actividad"));

                    cells.Colspan = 6;
                    cells.Border = 0;
                    cells.HorizontalAlignment = 0;
                    tabla.AddCell(cells);



                    BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
                    iTextSharp.text.Font times =
                      new iTextSharp.text.Font(bfTimes, 13, iTextSharp.text.Font.NORMAL);
                    foreach (DataRow dr in dt.Rows)
                    {
                        tabla.AddCell(new Phrase(dr["nomcli"].ToString(), times));
                        tabla.AddCell(new Phrase(dr["apellcli"].ToString(), times));
                        tabla.AddCell(new Phrase(dr["dni"].ToString(), times));
                        tabla.AddCell(new Phrase(dr["edad"].ToString(), times));
                        tabla.AddCell(new Phrase(dr["nomact"].ToString(), times));
                        tabla.AddCell(new Phrase(dr["nomape"].ToString(), times));
                    }
                    doc.Add(tabla);
                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo.FileName = "Clientes por actividad.pdf";
                    proc.Start();
                    proc.Close();
                    doc.Close();
                    writer.Close();
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void PDFXNUTRICIONISTA()
        {
            try
            {
                string hrSQL = ApTables.PDFNUTRI();
                dt = new DataTable();
                objDatos = new DatosMySQL();

                dt = objDatos.traerDataTable(hrSQL, cadCn);
                if (dt == null)
                {
                    MessageBox.Show("No hay turnos");
                }
                else
                {
                    Document doc = new Document(PageSize.A4);
                    PdfWriter writer = PdfWriter.GetInstance(doc, new System.IO.FileStream("Turnos del nutricionista.pdf", System.IO.FileMode.Create));

                    doc.AddTitle("Nutricionista");
                    doc.AddAuthor("Facundo Arguello");

                    doc.Open();

                    PdfPTable tabla = new PdfPTable(4);
                    tabla.TotalWidth = 550;
                    tabla.LockedWidth = true;

                    float[] widths = new float[] { 8f, 8f, 8f, 5f };

                    tabla.SetWidths(widths);
                    tabla.HorizontalAlignment = 0;

                    tabla.SpacingBefore = 30f;
                    tabla.SpacingAfter = 60f;

                    PdfPCell cells = new PdfPCell(new Phrase("Lista de turnos"));

                    cells.Colspan = 4;
                    cells.Border = 0;
                    cells.HorizontalAlignment = 0;
                    tabla.AddCell(cells);



                    BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
                    iTextSharp.text.Font times =
                      new iTextSharp.text.Font(bfTimes, 13, iTextSharp.text.Font.NORMAL);
                    foreach (DataRow dr in dt.Rows)
                    {
                        tabla.AddCell(new Phrase(dr["dniclien"].ToString(), times));
                        tabla.AddCell(new Phrase(dr["nomape"].ToString(), times));
                        tabla.AddCell(new Phrase(dr["fecha"].ToString(), times));
                        tabla.AddCell(new Phrase(dr["rango"].ToString(), times));

                    }
                    doc.Add(tabla);
                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo.FileName = "Turnos del nutricionista.pdf";
                    proc.Start();
                    proc.Close();
                    doc.Close();
                    writer.Close();
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void Consultar_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                PDFexp();
            }
            if(radioButton2.Checked)
            {
                PDFACT();
            }
            if (radioButton3.Checked)
            {
                PDFACTxCLI();
            }
            if(radioButton4.Checked)
            {
                PDFXNUTRICIONISTA();
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
            {
                groupBox1.Enabled = true;              
            }
            if (radioButton3.Checked == false)
            {
                groupBox1.Enabled = false;
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            obprofxact();
        }

        private void btcancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
