﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//
using ConectorBD;
using System.Configuration;
using AForge.Video.DirectShow;
using BarcodeLib.BarcodeReader;


namespace Gymtesis
{
    public partial class IngresoClientes : Form
    {
        DatosMySQL objDatos;
        DataTable dt;
        string cadCn;
        private FilterInfoCollection dispositivo;
        private VideoCaptureDevice fuentedevideo;
        public IngresoClientes()
        {
            InitializeComponent();
        }
        private void IngresoClientes_Load(object sender, EventArgs e)
        {
            cadCn = ConfigurationManager.ConnectionStrings["MYSQL"].ConnectionString;
            bsIngreso.DataSource = dt;
            dgvingreso.DataSource = bsIngreso;
            obtenerclientes();           
            combocamara();
            deslizador.Height = button2.Height;
            deslizador.Top = button2.Top;
        }

        private void label1_Click(object sender, EventArgs e)
        {
           
        }
        private void obtenerclientes()
        {
            string qrSQL = ApTables.expiracion();
            objDatos = new DatosMySQL();
            dt = new DataTable();

            dt = objDatos.traerDataTable(qrSQL, cadCn);

            if (dt == null || dt.Rows.Count == 0)
                bsIngreso.DataSource = null;
            else
                bsIngreso.DataSource = dt;
        }
        public void empezaraleer()
        {
            timer1.Enabled = true;
            fuentedevideo = new VideoCaptureDevice(dispositivo[cbcam.SelectedIndex].MonikerString);

            Lector.VideoSource = fuentedevideo;
            Lector.Start();
        }

        public void combocamara()
        {
            dispositivo = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo x in dispositivo)
            {
                cbcam.Items.Add(x.Name);
            }
            cbcam.SelectedIndex = 0;
        }
        private void Lector_Click(object sender, EventArgs e)
        {
            
        }
        public void dejardeleer()
        {
            timer1.Enabled = false;
            Lector.SignalToStop();
        }
        private void leerqrExpiracion()
        {
            string feSQL = "select c.dni, c.nomcli, c.apellcli, datediff(c.feexp, curdate()) AS expiracion , a.nomact, p.nomape  from gimnasio.cliente c inner join actividad a on c.idact = a.idactividad inner join profesionales p on c.idprof = p.idprof where c.dni = " + txtdniing.Text;
           DataTable dt1 = new DataTable();
            objDatos = new DatosMySQL();

            dt1 = objDatos.traerDataTable(feSQL, cadCn);

            foreach (DataGridViewRow fila in dgvingreso.Rows)
            {
                
                if (dt1 == null) return;
                //if (txtdniing == null) return;
                string dni = dt1.Rows[0]["dni"].ToString();
                string nom = dt1.Rows[0]["nomcli"].ToString();
                string ape = dt1.Rows[0]["apellcli"].ToString();
                string exp = dt1.Rows[0]["expiracion"].ToString();
                string act = dt1.Rows[0]["nomact"].ToString();
                string nompr = dt1.Rows[0]["nomape"].ToString();
                if (dni == txtdniing.Text)
                {

                    if (Convert.ToInt32(dt1.Rows[0]["expiracion"]) < 0)
                    {
                        mu.label1.Text = nom;
                        mu.label2.Text = ape;
                        mu.label3.Text = dni;
                        mu.label4.Text = exp;
                        mu.label10.Text = act;
                        mu.label12.Text = nompr;
                        mu.BackColor = System.Drawing.Color.Red;
                        txtdniing.Text = "";
                        dejardeleer();
                        mu.ShowDialog();
                        empezaraleer();
                    }
                    else if (Convert.ToInt32 (dt1.Rows[0]["expiracion"]) <= 3)
                    {
                        mu.label1.Text = nom;
                        mu.label2.Text = ape;
                        mu.label3.Text = dni;
                        mu.label4.Text = exp;
                        mu.label10.Text = act;
                        mu.label12.Text = nompr;
                        mu.BackColor = System.Drawing.Color.Yellow;
                        txtdniing.Text = "";
                        dejardeleer();
                        mu.ShowDialog();
                        empezaraleer();
                    }

                    else
                    {
                        mu.label1.Text = nom;
                        mu.label2.Text = ape;
                        mu.label3.Text = dni;
                        mu.label4.Text = exp;
                        mu.label10.Text = act;
                        mu.label12.Text = nompr;
                        mu.BackColor = System.Drawing.Color.Green;
                        txtdniing.Text = "";
                        dejardeleer();
                        mu.ShowDialog();
                        empezaraleer();

                    }
                }
            }
        }
        Muestra mu = new Muestra();
        private void timer1_Tick(object sender, EventArgs e)
        {
            //ESTAR SEGUROS QUE HAY UNA IMAGEN EN LA WEBCAM
            if (Lector.GetCurrentVideoFrame() != null)
            {
                //OBTENER IMAGEN DE LA WEBCAM
                Bitmap img = new Bitmap(Lector.GetCurrentVideoFrame());
                //UTILIZAR LA LIBRERIA PARA LEER LOS CODIGOS
                string[] resultados = BarcodeReader.read(img, BarcodeReader.QRCODE);
                img.Dispose();
                //OBTENER RESULTADOS DESP DE LA LECTURA
                if (resultados != null && resultados.Count() > 0)
                {
                    //AGREGAR EN EL TEXT BOX LO OBTENIDO
                    txtdniing.Text = resultados[0];
                    leerqrExpiracion();
                }

                
            }
        }
        
                      
        private void txtdniing_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {

                e.Handled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataView dv = dt.DefaultView;


            if (txtdniing.Text.Trim() != "")
            {
                dv.RowFilter = "dni=" + txtdniing.Text;              
                //leerqrExpiracion();
                //dejardeleer();
                txtdniing.Text = "";
            }

            else

                dv.RowFilter = "";

                dgvingreso.DataSource = dv;
                      
        }
        private void button2_Click(object sender, EventArgs e)
        {
            empezaraleer();
            deslizador.Height = button2.Height;
            deslizador.Top = button2.Top;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            dejardeleer();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dejardeleer();
            deslizador.Height = button3.Height;
            deslizador.Top = button3.Top;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            dejardeleer();
            this.Close();
        }
        int mousex = 0, mousey = 0;
        bool mouseDoewn;

        private void panel2_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDoewn = false;
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDoewn)
            {
                mousex = MousePosition.X - 200;
                mousey = MousePosition.Y - 40;

                this.SetDesktopLocation(mousex, mousey);
            }
        }

      
        private void dgvingreso_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Modificarcliente modcl = new Modificarcliente();
            modcl.dni = dgvingreso.Rows[e.RowIndex].Cells[0].Value.ToString();
            modcl.ShowDialog();
            obtenerclientes();
        }


        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDoewn = true;
        }

        

    }
}
