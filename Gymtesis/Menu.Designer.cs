﻿namespace Gymtesis
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.btingresoclientes = new System.Windows.Forms.Button();
            this.btturn = new System.Windows.Forms.Button();
            this.btregcli = new System.Windows.Forms.Button();
            this.btregprof = new System.Windows.Forms.Button();
            this.btagregprof = new System.Windows.Forms.Button();
            this.btagregcli = new System.Windows.Forms.Button();
            this.btagregturn = new System.Windows.Forms.Button();
            this.btacercade = new System.Windows.Forms.Button();
            this.btmanual = new System.Windows.Forms.Button();
            this.btsalir = new System.Windows.Forms.Button();
            this.border = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.btact = new System.Windows.Forms.Button();
            this.btformcon = new System.Windows.Forms.Button();
            this.border.SuspendLayout();
            this.SuspendLayout();
            // 
            // btingresoclientes
            // 
            this.btingresoclientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(131)))), ((int)(((byte)(235)))));
            this.btingresoclientes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btingresoclientes.FlatAppearance.BorderSize = 0;
            this.btingresoclientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btingresoclientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btingresoclientes.Image = ((System.Drawing.Image)(resources.GetObject("btingresoclientes.Image")));
            this.btingresoclientes.Location = new System.Drawing.Point(54, 88);
            this.btingresoclientes.Name = "btingresoclientes";
            this.btingresoclientes.Size = new System.Drawing.Size(130, 320);
            this.btingresoclientes.TabIndex = 2;
            this.btingresoclientes.Text = "Ingreso de clientes";
            this.btingresoclientes.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btingresoclientes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btingresoclientes.UseVisualStyleBackColor = false;
            this.btingresoclientes.Click += new System.EventHandler(this.btingresoclientes_Click);
            // 
            // btturn
            // 
            this.btturn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(196)))), ((int)(((byte)(15)))));
            this.btturn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btturn.FlatAppearance.BorderSize = 0;
            this.btturn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btturn.Image = ((System.Drawing.Image)(resources.GetObject("btturn.Image")));
            this.btturn.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btturn.Location = new System.Drawing.Point(462, 88);
            this.btturn.Name = "btturn";
            this.btturn.Size = new System.Drawing.Size(130, 100);
            this.btturn.TabIndex = 3;
            this.btturn.Text = "Turnero";
            this.btturn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btturn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btturn.UseVisualStyleBackColor = false;
            this.btturn.Click += new System.EventHandler(this.btturn_Click);
            // 
            // btregcli
            // 
            this.btregcli.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(163)))), ((int)(((byte)(0)))));
            this.btregcli.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btregcli.FlatAppearance.BorderSize = 0;
            this.btregcli.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btregcli.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btregcli.Image = ((System.Drawing.Image)(resources.GetObject("btregcli.Image")));
            this.btregcli.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btregcli.Location = new System.Drawing.Point(190, 87);
            this.btregcli.Name = "btregcli";
            this.btregcli.Size = new System.Drawing.Size(130, 159);
            this.btregcli.TabIndex = 4;
            this.btregcli.Text = "Registro de clientes";
            this.btregcli.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btregcli.UseVisualStyleBackColor = false;
            this.btregcli.Click += new System.EventHandler(this.btregcli_Click);
            // 
            // btregprof
            // 
            this.btregprof.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(104)))), ((int)(((byte)(0)))));
            this.btregprof.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btregprof.FlatAppearance.BorderSize = 0;
            this.btregprof.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btregprof.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btregprof.Image = ((System.Drawing.Image)(resources.GetObject("btregprof.Image")));
            this.btregprof.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btregprof.Location = new System.Drawing.Point(326, 87);
            this.btregprof.Name = "btregprof";
            this.btregprof.Size = new System.Drawing.Size(130, 159);
            this.btregprof.TabIndex = 5;
            this.btregprof.Text = "Registro de profesionales";
            this.btregprof.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btregprof.UseVisualStyleBackColor = false;
            this.btregprof.Click += new System.EventHandler(this.button7_Click);
            // 
            // btagregprof
            // 
            this.btagregprof.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(104)))), ((int)(((byte)(0)))));
            this.btagregprof.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btagregprof.FlatAppearance.BorderSize = 0;
            this.btagregprof.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btagregprof.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btagregprof.Image = ((System.Drawing.Image)(resources.GetObject("btagregprof.Image")));
            this.btagregprof.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btagregprof.Location = new System.Drawing.Point(326, 249);
            this.btagregprof.Name = "btagregprof";
            this.btagregprof.Size = new System.Drawing.Size(130, 159);
            this.btagregprof.TabIndex = 6;
            this.btagregprof.Text = "Agregar nuevo profesional";
            this.btagregprof.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btagregprof.UseVisualStyleBackColor = false;
            this.btagregprof.Click += new System.EventHandler(this.btagregprof_Click);
            // 
            // btagregcli
            // 
            this.btagregcli.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(163)))), ((int)(((byte)(0)))));
            this.btagregcli.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btagregcli.FlatAppearance.BorderSize = 0;
            this.btagregcli.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btagregcli.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btagregcli.Image = ((System.Drawing.Image)(resources.GetObject("btagregcli.Image")));
            this.btagregcli.Location = new System.Drawing.Point(190, 249);
            this.btagregcli.Name = "btagregcli";
            this.btagregcli.Size = new System.Drawing.Size(130, 159);
            this.btagregcli.TabIndex = 7;
            this.btagregcli.Text = "Agrega cliente";
            this.btagregcli.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btagregcli.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btagregcli.UseVisualStyleBackColor = false;
            this.btagregcli.Click += new System.EventHandler(this.btagregcli_Click);
            // 
            // btagregturn
            // 
            this.btagregturn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(196)))), ((int)(((byte)(15)))));
            this.btagregturn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btagregturn.FlatAppearance.BorderSize = 0;
            this.btagregturn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btagregturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btagregturn.Image = ((System.Drawing.Image)(resources.GetObject("btagregturn.Image")));
            this.btagregturn.Location = new System.Drawing.Point(462, 190);
            this.btagregturn.Name = "btagregturn";
            this.btagregturn.Size = new System.Drawing.Size(130, 101);
            this.btagregturn.TabIndex = 8;
            this.btagregturn.Text = "Agregar turno";
            this.btagregturn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btagregturn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btagregturn.UseVisualStyleBackColor = false;
            this.btagregturn.Click += new System.EventHandler(this.btagregturn_Click);
            // 
            // btacercade
            // 
            this.btacercade.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(135)))), ((int)(((byte)(100)))));
            this.btacercade.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btacercade.FlatAppearance.BorderSize = 0;
            this.btacercade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btacercade.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btacercade.Image = ((System.Drawing.Image)(resources.GetObject("btacercade.Image")));
            this.btacercade.Location = new System.Drawing.Point(462, 296);
            this.btacercade.Name = "btacercade";
            this.btacercade.Size = new System.Drawing.Size(130, 112);
            this.btacercade.TabIndex = 10;
            this.btacercade.Text = "Acerca de";
            this.btacercade.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btacercade.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btacercade.UseVisualStyleBackColor = false;
            this.btacercade.Click += new System.EventHandler(this.btacercade_Click);
            // 
            // btmanual
            // 
            this.btmanual.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(135)))), ((int)(((byte)(100)))));
            this.btmanual.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btmanual.FlatAppearance.BorderSize = 0;
            this.btmanual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmanual.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmanual.Image = ((System.Drawing.Image)(resources.GetObject("btmanual.Image")));
            this.btmanual.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btmanual.Location = new System.Drawing.Point(598, 296);
            this.btmanual.Name = "btmanual";
            this.btmanual.Size = new System.Drawing.Size(130, 112);
            this.btmanual.TabIndex = 9;
            this.btmanual.Text = "Manual de usuario";
            this.btmanual.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btmanual.UseVisualStyleBackColor = false;
            this.btmanual.Click += new System.EventHandler(this.btmanual_Click);
            // 
            // btsalir
            // 
            this.btsalir.BackColor = System.Drawing.Color.Transparent;
            this.btsalir.Cursor = System.Windows.Forms.Cursors.Default;
            this.btsalir.FlatAppearance.BorderSize = 0;
            this.btsalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btsalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btsalir.ForeColor = System.Drawing.Color.White;
            this.btsalir.Image = ((System.Drawing.Image)(resources.GetObject("btsalir.Image")));
            this.btsalir.Location = new System.Drawing.Point(684, 429);
            this.btsalir.Name = "btsalir";
            this.btsalir.Size = new System.Drawing.Size(108, 93);
            this.btsalir.TabIndex = 11;
            this.btsalir.Text = "Salir";
            this.btsalir.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btsalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btsalir.UseVisualStyleBackColor = false;
            this.btsalir.Click += new System.EventHandler(this.btsalir_Click);
            // 
            // border
            // 
            this.border.BackColor = System.Drawing.Color.Red;
            this.border.Controls.Add(this.button1);
            this.border.Dock = System.Windows.Forms.DockStyle.Top;
            this.border.Location = new System.Drawing.Point(0, 0);
            this.border.Name = "border";
            this.border.Size = new System.Drawing.Size(792, 33);
            this.border.TabIndex = 12;
            this.border.MouseDown += new System.Windows.Forms.MouseEventHandler(this.border_MouseDown);
            this.border.MouseMove += new System.Windows.Forms.MouseEventHandler(this.border_MouseMove);
            this.border.MouseUp += new System.Windows.Forms.MouseEventHandler(this.border_MouseUp);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(751, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(38, 27);
            this.button1.TabIndex = 0;
            this.button1.Text = "X";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btact
            // 
            this.btact.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(76)))), ((int)(((byte)(60)))));
            this.btact.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btact.FlatAppearance.BorderSize = 0;
            this.btact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btact.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btact.Image = ((System.Drawing.Image)(resources.GetObject("btact.Image")));
            this.btact.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btact.Location = new System.Drawing.Point(598, 88);
            this.btact.Name = "btact";
            this.btact.Size = new System.Drawing.Size(130, 111);
            this.btact.TabIndex = 13;
            this.btact.Text = "Actividades";
            this.btact.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btact.UseVisualStyleBackColor = false;
            this.btact.Click += new System.EventHandler(this.btact_Click);
            // 
            // btformcon
            // 
            this.btformcon.BackColor = System.Drawing.Color.White;
            this.btformcon.FlatAppearance.BorderSize = 0;
            this.btformcon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btformcon.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btformcon.Image = ((System.Drawing.Image)(resources.GetObject("btformcon.Image")));
            this.btformcon.Location = new System.Drawing.Point(598, 202);
            this.btformcon.Name = "btformcon";
            this.btformcon.Size = new System.Drawing.Size(130, 89);
            this.btformcon.TabIndex = 14;
            this.btformcon.Text = "Consultas";
            this.btformcon.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btformcon.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btformcon.UseVisualStyleBackColor = false;
            this.btformcon.Click += new System.EventHandler(this.btformcon_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(0)))), ((int)(((byte)(84)))));
            this.ClientSize = new System.Drawing.Size(792, 520);
            this.Controls.Add(this.btformcon);
            this.Controls.Add(this.btact);
            this.Controls.Add(this.border);
            this.Controls.Add(this.btsalir);
            this.Controls.Add(this.btacercade);
            this.Controls.Add(this.btmanual);
            this.Controls.Add(this.btagregturn);
            this.Controls.Add(this.btagregcli);
            this.Controls.Add(this.btagregprof);
            this.Controls.Add(this.btregprof);
            this.Controls.Add(this.btregcli);
            this.Controls.Add(this.btturn);
            this.Controls.Add(this.btingresoclientes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Menu";
            this.Text = "Menu";
            this.Load += new System.EventHandler(this.Menu_Load);
            this.border.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btingresoclientes;
        private System.Windows.Forms.Button btturn;
        private System.Windows.Forms.Button btregcli;
        private System.Windows.Forms.Button btregprof;
        private System.Windows.Forms.Button btagregprof;
        private System.Windows.Forms.Button btagregcli;
        private System.Windows.Forms.Button btagregturn;
        private System.Windows.Forms.Button btacercade;
        private System.Windows.Forms.Button btmanual;
        private System.Windows.Forms.Button btsalir;
        private System.Windows.Forms.Panel border;
        private System.Windows.Forms.Button btact;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btformcon;
    }
}