﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gymtesis
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void profesoresToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        private void button7_Click(object sender, EventArgs e)
        {
            ABMProf prof = new ABMProf();
            prof.Show();
        }

        private void btingresoclientes_Click(object sender, EventArgs e)
        {
            IngresoClientes ic = new IngresoClientes();
            ic.Show();
        }

        private void btregcli_Click(object sender, EventArgs e)
        {
            ABMcliente regcli = new ABMcliente();
            regcli.Show();
        }

        private void btagregcli_Click(object sender, EventArgs e)
        {
            Agregarcliente agregcli = new Agregarcliente();
            agregcli.ShowDialog();
        }

        private void btagregprof_Click(object sender, EventArgs e)
        {
            AgregarProf agregprof = new AgregarProf();
            agregprof.ShowDialog();
        }

        private void btturn_Click(object sender, EventArgs e)
        {
            Turnero turn = new Turnero();
            turn.Show();
        }

        private void btagregturn_Click(object sender, EventArgs e)
        {
            NuevoTurno nt = new NuevoTurno();
            nt.ShowDialog();
        }

        private void btacercade_Click(object sender, EventArgs e)
        {
            AboutBox1 acer = new AboutBox1();
            acer.Show();
        }

        private void btact_Click(object sender, EventArgs e)
        {
            ABMact act = new ABMact();
            act.Show();
        }

        private void btsalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        int mousex = 0, mousey = 0;
        bool mouseDoewn;

        private void border_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDoewn = true;
        }

        private void border_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDoewn = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Menu_Load(object sender, EventArgs e)
        {

        }

        private void btmanual_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo.FileName = "Manual De Usuario.signed.pdf";
            proc.Start();
            proc.Close();
        }

        private void btformcon_Click(object sender, EventArgs e)
        {
            Consultas con = new Consultas();
            con.Show();
        }

        private void border_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDoewn)
            {
                mousex = MousePosition.X - 200;
                mousey = MousePosition.Y - 40;

                this.SetDesktopLocation(mousex, mousey);
            }
        }
    }
}
