﻿namespace Gymtesis
{
    partial class ModAct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModAct));
            this.label1 = new System.Windows.Forms.Label();
            this.txtnomactmod = new System.Windows.Forms.TextBox();
            this.txtidact = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btcanmodact = new System.Windows.Forms.Button();
            this.txtpreciomod = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtcapmod = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 16);
            this.label1.TabIndex = 12;
            this.label1.Text = "Nombre";
            // 
            // txtnomactmod
            // 
            this.txtnomactmod.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtnomactmod.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnomactmod.Location = new System.Drawing.Point(12, 74);
            this.txtnomactmod.Name = "txtnomactmod";
            this.txtnomactmod.Size = new System.Drawing.Size(100, 22);
            this.txtnomactmod.TabIndex = 8;
            // 
            // txtidact
            // 
            this.txtidact.Enabled = false;
            this.txtidact.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtidact.Location = new System.Drawing.Point(201, 24);
            this.txtidact.Name = "txtidact";
            this.txtidact.Size = new System.Drawing.Size(55, 22);
            this.txtidact.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(156, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 16);
            this.label5.TabIndex = 17;
            this.label5.Text = "ID";
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(98, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 54);
            this.button1.TabIndex = 18;
            this.button1.Text = "Modificar";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btcanmodact
            // 
            this.btcanmodact.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btcanmodact.FlatAppearance.BorderSize = 0;
            this.btcanmodact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btcanmodact.Image = ((System.Drawing.Image)(resources.GetObject("btcanmodact.Image")));
            this.btcanmodact.Location = new System.Drawing.Point(222, 12);
            this.btcanmodact.Name = "btcanmodact";
            this.btcanmodact.Size = new System.Drawing.Size(125, 54);
            this.btcanmodact.TabIndex = 19;
            this.btcanmodact.Text = "Cancelar";
            this.btcanmodact.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btcanmodact.UseVisualStyleBackColor = true;
            this.btcanmodact.Click += new System.EventHandler(this.btcanmodact_Click);
            // 
            // txtpreciomod
            // 
            this.txtpreciomod.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpreciomod.Location = new System.Drawing.Point(159, 74);
            this.txtpreciomod.MaxLength = 6;
            this.txtpreciomod.Name = "txtpreciomod";
            this.txtpreciomod.Size = new System.Drawing.Size(100, 22);
            this.txtpreciomod.TabIndex = 21;
            this.txtpreciomod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtpreciomod_KeyPress);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(21)))), ((int)(((byte)(34)))));
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btcanmodact);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.ForeColor = System.Drawing.SystemColors.Control;
            this.panel1.Location = new System.Drawing.Point(0, 137);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(421, 69);
            this.panel1.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(168, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 16);
            this.label6.TabIndex = 24;
            this.label6.Text = "Precio";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(302, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 16);
            this.label2.TabIndex = 26;
            this.label2.Text = "Capacidad";
            // 
            // txtcapmod
            // 
            this.txtcapmod.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcapmod.Location = new System.Drawing.Point(293, 74);
            this.txtcapmod.MaxLength = 6;
            this.txtcapmod.Name = "txtcapmod";
            this.txtcapmod.Size = new System.Drawing.Size(100, 22);
            this.txtcapmod.TabIndex = 25;
            this.txtcapmod.TextChanged += new System.EventHandler(this.txtcapmod_TextChanged);
            this.txtcapmod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcapmod_KeyPress);
            // 
            // ModAct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(421, 206);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtcapmod);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtpreciomod);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtidact);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtnomactmod);
            this.Name = "ModAct";
            this.Text = "ModAct";
            this.Load += new System.EventHandler(this.ModAct_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtnomactmod;
        private System.Windows.Forms.TextBox txtidact;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btcanmodact;
        private System.Windows.Forms.TextBox txtpreciomod;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtcapmod;
    }
}