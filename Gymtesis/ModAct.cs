﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//
using ConectorBD;
using System.Configuration;

namespace Gymtesis
{
    public partial class ModAct : Form
    {
        DatosMySQL objDatos;
        DataTable dt;
        string cadCn;
        public string idact { get; set; }
        int valor;
        public ModAct()
        {
            InitializeComponent();
        }

        private void btcanmodact_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ModAct_Load(object sender, EventArgs e)
        {
            cadCn = ConfigurationManager.ConnectionStrings["MYSQL"].ConnectionString;
            pasaract();
        }
        private void pasaract()
        {
            string modASQL = "select * from  gimnasio.actividad where idactividad=" + idact;
            objDatos = new DatosMySQL();
            dt = new DataTable();

            dt = objDatos.traerDataTable(modASQL, cadCn);

            if (dt == null) return;
            {
                txtidact.Text = dt.Rows[0]["idactividad"].ToString();
                txtnomactmod.Text = dt.Rows[0]["nomact"].ToString();
                txtcapmod.Text = dt.Rows[0]["capacidad"].ToString();               
                txtpreciomod.Text = dt.Rows[0]["precio"].ToString();
                
            }
        }
        private bool Validaractmod()
        {
            if (txtnomactmod.Text == "")
            {
                MessageBox.Show("Debe ingresar el nombre de la actividad");
                txtnomactmod.Focus();
                return false;
            }
            if (txtpreciomod.Text == "")
            {
                MessageBox.Show("Debe ingresar el precio de lactividad");
                txtpreciomod.Focus();
                return false;
            }
            
            return true;
        }
        //private void obcombpro( string ida)
        //{
        //    string cmpSQL = ApTables.BDcomboprof();
        //    objDatos = new DatosMySQL();
        //    dt = new DataTable();

        //    dt = objDatos.traerDataTable(cmpSQL, cadCn);

        //    if(dt!= null)
        //    {
        //        comboprof.ValueMember = "idprof";
        //        comboprof.DisplayMember = "nomape";
        //        comboprof.DataSource = dt;
        //    }
        //    comboprof.SelectedValue = ida;
        //}
        private void modificarBDACT()
        {
            string mactSQL = "update gimnasio.actividad set nomact='" + txtnomactmod.Text + "', precio ='" + txtpreciomod.Text + "' , capacidad= " + txtcapmod.Text +
                " where idactividad =" + txtidact.Text + ";";
            objDatos = new DatosMySQL();
            valor = objDatos.ejecutarSQL(mactSQL, cadCn);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Validaractmod() == true)
            {
                modificarBDACT();
                this.Close();
            }
        }

        private void txtpreciomod_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtcapmod_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtcapmod_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
               if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
