﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//
using ConectorBD;
using System.Configuration;

namespace Gymtesis
{
    public partial class Modificarcliente : Form
    {
        DatosMySQL objDatos;
        DataTable dt;
        int valor;
        string cadCn;
        public string dni { get; set; }

        public Modificarcliente()
        {
            InitializeComponent();
        }
        private void Modificarcliente_Load(object sender, EventArgs e)
        {
            cadCn = ConfigurationManager.ConnectionStrings["MYSQL"].ConnectionString;
            doc();
            llenarlabelsmod();
            
        }
        private void doc()
        {
            string dnisql = "select * from gimnasio.cliente where idcliente = " + dni;
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(dnisql, cadCn);

            if (dt == null) return;
            string cbprof = dt.Rows[0]["idprof"].ToString();
            textBox6.Text = dt.Rows[0]["idcliente"].ToString();
                textBox1.Text = dt.Rows[0]["nomcli"].ToString();
                textBox2.Text = dt.Rows[0]["apellcli"].ToString();
                textBox3.Text = dt.Rows[0]["dni"].ToString();
                textBox4.Text = dt.Rows[0]["tel"].ToString();
                textBox5.Text = dt.Rows[0]["edad"].ToString();           
                dtpi.Text = dt.Rows[0]["fepago"].ToString();
                dtpe.Text = dt.Rows[0]["feexp"].ToString();
                obxact(dt.Rows[0]["idact"].ToString());
            obxprof(cbprof);
        }
        
        private void modificarcli()
        {
            string fechaUno = (Convert.ToDateTime(dtpi.Text)).ToString("yyyy-MM-dd");
            string fechaDos = (Convert.ToDateTime(dtpe.Text)).ToString("yyyy-MM-dd");

            string modSQL = "update gimnasio.cliente set nomcli = '" + textBox1.Text + "',apellcli= '" + textBox2.Text +
                "',dni= " + textBox3.Text + ", tel= " + textBox4.Text + ",edad= " + textBox5.Text +
                ", idact= " + Convert.ToUInt32(comboBox1.SelectedValue) + ", idprof= " + Convert.ToInt32(cbprofesorm.SelectedValue) + ", fepago= '" + fechaUno +
                "', feexp= '" + fechaDos + "' where idcliente = " + textBox6.Text + ";";

            objDatos = new DatosMySQL();

            valor = objDatos.ejecutarSQL(modSQL, cadCn);

            this.Close();
        }
        private void obprofesormod()
        {
            DataRowView dr = (DataRowView)comboBox1.SelectedItem;
            int id = Convert.ToInt32(dr[0]);
            string prfSQL = "select * from gimnasio.profesionales where idact = " + id.ToString() + "";
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(prfSQL, cadCn);

            if (dt == null) return;
            {
                cbprofesorm.ValueMember = "idprof";
                cbprofesorm.DisplayMember = "nomape";
                cbprofesorm.DataSource = dt;
            }
        }
        private void llenarlabelsmod()
        {

            string labSQL = "select d.Dias, h.Horarios from gimnasio.profesionales p inner join diasprof d on p.iddia = d.iddia inner join horariosprof h on p.idhorario = h.idhorp where p.idprof = " + cbprofesorm.SelectedValue;
            dt = new DataTable();
            objDatos = new DatosMySQL();
            dt = objDatos.traerDataTable(labSQL, cadCn);
            if (dt == null) return;
            label9.Text = dt.Rows[0]["Dias"].ToString();
            label10.Text = dt.Rows[0]["Horarios"].ToString();
        }
        private void llenarpreciolabelmod()
        {           
            string preSQL = "select precio from gimnasio.actividad where idactividad=" + comboBox1.SelectedValue;
            dt = new DataTable();
            objDatos = new DatosMySQL();
            dt = objDatos.traerDataTable(preSQL, cadCn);
            if (dt == null) return;
            labelprecio.Text = dt.Rows[0]["precio"].ToString();
        }
        private void obxact(string ida)
        {
            string aSQL = ApTables.comboact();
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(aSQL, cadCn);
            if (dt != null)
            {
                comboBox1.ValueMember = "idactividad";
                comboBox1.DisplayMember = "nomact";
                comboBox1.DataSource = dt;
            }
            comboBox1.SelectedValue = ida;
        }
        private void obxprof(string idap)
        {
            string profmSQL = ApTables.comboProfformmodclientes();
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(profmSQL, cadCn);
            if (dt != null)
            {
                cbprofesorm.ValueMember = "idprof";
                cbprofesorm.DisplayMember = "nomape";
                cbprofesorm.DataSource = dt;
            }
            cbprofesorm.SelectedValue = idap;
        }
        private void CapacidaddeActMod()
        {
          
            string capSQL = "select a.capacidad, count(*) as cantidad " +
                "from cliente c inner join actividad a on c.idact = a.idactividad" +
                " where c.idact =" + Convert.ToInt32(comboBox1.SelectedValue) + " and c.idprof =" + Convert.ToInt32(cbprofesorm.SelectedValue);
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(capSQL, cadCn);
            if (dt == null) return;
            int cap = Convert.ToInt32(dt.Rows[0]["capacidad"]);
            int cant = Convert.ToInt32(dt.Rows[0]["cantidad"]);
            if (cap > cant)
            {
                MessageBox.Show("Hay capacidad");
                modificarcli();              
                this.Close();
            }
            else
            {
                MessageBox.Show("No hay cupo");
            }
        }
        private void btcanmod_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private bool validar()
        {
            if(textBox1.Text == "")
            {
                MessageBox.Show("Debe ingresar nombre");
                textBox1.Focus();
                return false;
            }
            if (textBox2.Text == "")
            {
                MessageBox.Show("Debe ingresar apellido");
                textBox2.Focus();
                return false;
            }
            if (textBox3.Text == "")
            {
                MessageBox.Show("Debe ingresar el DNI");
                textBox3.Focus();
                return false;
            }
            if (textBox4.Text == "")
            {
                MessageBox.Show("Debe ingresar el DNI");
                textBox4.Focus();
                return false;
            }
            if (textBox5.Text == "")
            {
                MessageBox.Show("Debe ingresar la edad");
                textBox5.Focus();
                return false;
            }
            return true;
        }
        private void btmodc_Click(object sender, EventArgs e)
        {
            if (validar() == true)
            {
                CapacidaddeActMod();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            obprofesormod();
            llenarpreciolabelmod();
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void cbprofesorm_SelectedIndexChanged(object sender, EventArgs e)
        {
            llenarlabelsmod();
        }

        private void cbprofesorm_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string msgcapmSQL = "select a.capacidad, count(*) as cantidad " +
                "from cliente c inner join actividad a on c.idact = a.idactividad" +
                " where c.idact =" + Convert.ToInt32(comboBox1.SelectedValue) + " and c.idprof =" + Convert.ToInt32(cbprofesorm.SelectedValue);
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(msgcapmSQL, cadCn);
            if (dt == null) return;
            int cap = Convert.ToInt32(dt.Rows[0]["capacidad"]);
            int cant = Convert.ToInt32(dt.Rows[0]["cantidad"]);
            if (cap > cant)
            {
                MessageBox.Show("Hay capacidad");
            }
            else
            {
                MessageBox.Show("No hay cupo");
            }
        }
    }
}
