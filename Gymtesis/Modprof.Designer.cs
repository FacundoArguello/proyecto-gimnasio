﻿namespace Gymtesis
{
    partial class Modprof
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Modprof));
            this.button2 = new System.Windows.Forms.Button();
            this.btmodmprof = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.cbprov = new System.Windows.Forms.ComboBox();
            this.cbloc = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtnromp = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.cbactmp = new System.Windows.Forms.ComboBox();
            this.txtmail = new System.Windows.Forms.TextBox();
            this.txtdirmp = new System.Windows.Forms.TextBox();
            this.txttelmp = new System.Windows.Forms.TextBox();
            this.txtdnimp = new System.Windows.Forms.TextBox();
            this.txtna = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtidmodprof = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.cbhorpm = new System.Windows.Forms.ComboBox();
            this.cbdiam = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(282, 15);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(117, 60);
            this.button2.TabIndex = 65;
            this.button2.Text = "Cancelar";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btmodmprof
            // 
            this.btmodmprof.FlatAppearance.BorderSize = 0;
            this.btmodmprof.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmodmprof.Image = ((System.Drawing.Image)(resources.GetObject("btmodmprof.Image")));
            this.btmodmprof.Location = new System.Drawing.Point(133, 15);
            this.btmodmprof.Name = "btmodmprof";
            this.btmodmprof.Size = new System.Drawing.Size(117, 60);
            this.btmodmprof.TabIndex = 64;
            this.btmodmprof.Text = "Modificar";
            this.btmodmprof.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btmodmprof.UseVisualStyleBackColor = true;
            this.btmodmprof.Click += new System.EventHandler(this.btmodmprof_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(87, 282);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 13);
            this.label11.TabIndex = 63;
            this.label11.Text = "Provincia";
            // 
            // cbprov
            // 
            this.cbprov.FormattingEnabled = true;
            this.cbprov.Location = new System.Drawing.Point(151, 278);
            this.cbprov.Name = "cbprov";
            this.cbprov.Size = new System.Drawing.Size(121, 21);
            this.cbprov.TabIndex = 62;
            this.cbprov.SelectedIndexChanged += new System.EventHandler(this.cbprov_SelectedIndexChanged);
            // 
            // cbloc
            // 
            this.cbloc.FormattingEnabled = true;
            this.cbloc.Location = new System.Drawing.Point(353, 282);
            this.cbloc.Name = "cbloc";
            this.cbloc.Size = new System.Drawing.Size(121, 21);
            this.cbloc.TabIndex = 61;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(294, 286);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.TabIndex = 60;
            this.label10.Text = "Localidad";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(343, 234);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 13);
            this.label9.TabIndex = 59;
            this.label9.Text = "Numero";
            // 
            // txtnromp
            // 
            this.txtnromp.Location = new System.Drawing.Point(404, 231);
            this.txtnromp.MaxLength = 6;
            this.txtnromp.Name = "txtnromp";
            this.txtnromp.Size = new System.Drawing.Size(82, 20);
            this.txtnromp.TabIndex = 58;
            this.txtnromp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnromp_KeyPress);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(149, 182);
            this.dateTimePicker1.MaxDate = new System.DateTime(2018, 1, 1, 0, 0, 0, 0);
            this.dateTimePicker1.MinDate = new System.DateTime(1930, 1, 1, 0, 0, 0, 0);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 57;
            // 
            // cbactmp
            // 
            this.cbactmp.FormattingEnabled = true;
            this.cbactmp.Location = new System.Drawing.Point(149, 380);
            this.cbactmp.Name = "cbactmp";
            this.cbactmp.Size = new System.Drawing.Size(137, 21);
            this.cbactmp.TabIndex = 56;
            // 
            // txtmail
            // 
            this.txtmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmail.Location = new System.Drawing.Point(149, 330);
            this.txtmail.Name = "txtmail";
            this.txtmail.Size = new System.Drawing.Size(183, 20);
            this.txtmail.TabIndex = 54;
            this.txtmail.Leave += new System.EventHandler(this.txtmail_Leave);
            // 
            // txtdirmp
            // 
            this.txtdirmp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdirmp.Location = new System.Drawing.Point(149, 231);
            this.txtdirmp.Name = "txtdirmp";
            this.txtdirmp.Size = new System.Drawing.Size(183, 20);
            this.txtdirmp.TabIndex = 53;
            // 
            // txttelmp
            // 
            this.txttelmp.Location = new System.Drawing.Point(151, 134);
            this.txttelmp.MaxLength = 15;
            this.txttelmp.Name = "txttelmp";
            this.txttelmp.Size = new System.Drawing.Size(183, 20);
            this.txttelmp.TabIndex = 52;
            this.txttelmp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txttelmp_KeyPress);
            // 
            // txtdnimp
            // 
            this.txtdnimp.Location = new System.Drawing.Point(151, 88);
            this.txtdnimp.MaxLength = 8;
            this.txtdnimp.Name = "txtdnimp";
            this.txtdnimp.Size = new System.Drawing.Size(183, 20);
            this.txtdnimp.TabIndex = 51;
            this.txtdnimp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtdnimp_KeyPress);
            // 
            // txtna
            // 
            this.txtna.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtna.Location = new System.Drawing.Point(149, 45);
            this.txtna.Name = "txtna";
            this.txtna.Size = new System.Drawing.Size(183, 20);
            this.txtna.TabIndex = 50;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(87, 333);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 13);
            this.label7.TabIndex = 48;
            this.label7.Text = "Mail";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(66, 383);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 47;
            this.label6.Text = "Actividad";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 178);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 13);
            this.label5.TabIndex = 46;
            this.label5.Text = "Fecha de nacimiento";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(66, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 45;
            this.label4.Text = "Telefono";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(69, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 44;
            this.label3.Text = "DNI";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 230);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 43;
            this.label2.Text = "Direccion";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 42;
            this.label1.Text = "Nombre y apellido";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(77, 10);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(18, 13);
            this.label12.TabIndex = 66;
            this.label12.Text = "ID";
            // 
            // txtidmodprof
            // 
            this.txtidmodprof.Enabled = false;
            this.txtidmodprof.Location = new System.Drawing.Point(164, 9);
            this.txtidmodprof.Name = "txtidmodprof";
            this.txtidmodprof.Size = new System.Drawing.Size(69, 20);
            this.txtidmodprof.TabIndex = 67;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(104)))), ((int)(((byte)(0)))));
            this.panel1.Controls.Add(this.btmodmprof);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 452);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(513, 75);
            this.panel1.TabIndex = 68;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(320, 416);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 72;
            this.label8.Text = "Horario";
            // 
            // cbhorpm
            // 
            this.cbhorpm.FormattingEnabled = true;
            this.cbhorpm.Location = new System.Drawing.Point(380, 413);
            this.cbhorpm.Name = "cbhorpm";
            this.cbhorpm.Size = new System.Drawing.Size(121, 21);
            this.cbhorpm.TabIndex = 71;
            // 
            // cbdiam
            // 
            this.cbdiam.FormattingEnabled = true;
            this.cbdiam.Location = new System.Drawing.Point(380, 372);
            this.cbdiam.Name = "cbdiam";
            this.cbdiam.Size = new System.Drawing.Size(121, 21);
            this.cbdiam.TabIndex = 70;
            this.cbdiam.SelectedIndexChanged += new System.EventHandler(this.cbdiam_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(320, 375);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(28, 13);
            this.label13.TabIndex = 69;
            this.label13.Text = "Dias";
            // 
            // Modprof
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(513, 527);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cbhorpm);
            this.Controls.Add(this.cbdiam);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtidmodprof);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cbprov);
            this.Controls.Add(this.cbloc);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtnromp);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.cbactmp);
            this.Controls.Add(this.txtmail);
            this.Controls.Add(this.txtdirmp);
            this.Controls.Add(this.txttelmp);
            this.Controls.Add(this.txtdnimp);
            this.Controls.Add(this.txtna);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Modprof";
            this.Text = "Modprof";
            this.Load += new System.EventHandler(this.Modprof_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btmodmprof;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbprov;
        private System.Windows.Forms.ComboBox cbloc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtnromp;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox cbactmp;
        private System.Windows.Forms.TextBox txtmail;
        private System.Windows.Forms.TextBox txtdirmp;
        private System.Windows.Forms.TextBox txttelmp;
        private System.Windows.Forms.TextBox txtdnimp;
        private System.Windows.Forms.TextBox txtna;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtidmodprof;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbhorpm;
        private System.Windows.Forms.ComboBox cbdiam;
        private System.Windows.Forms.Label label13;
    }
}