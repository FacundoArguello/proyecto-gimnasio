﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//
using ConectorBD;
using System.Configuration;
using System.Text.RegularExpressions;

namespace Gymtesis
{
    public partial class Modprof : Form
    {
        public string idmod { get; set; }
        DatosMySQL objDatos;
        DataTable dt;
        string cadCn;
        int valor;
        public Modprof()
        {
            InitializeComponent();
        }

        private void Modprof_Load(object sender, EventArgs e)
        {
            cadCn = ConfigurationManager.ConnectionStrings["MYSQL"].ConnectionString;
            comboprov();
            pasdatos();
        }
        private void pasdatos()
        {
            string modSQL = "select * from gimnasio.profesionales where idprof=" + idmod;
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(modSQL, cadCn);

            if (dt == null) return;
            string md = dt.Rows[0]["loc"].ToString();
            string ac = dt.Rows[0]["idact"].ToString();
            string dia = dt.Rows[0]["iddia"].ToString();
            string hora = dt.Rows[0]["idhorario"].ToString();

            txtidmodprof.Text = dt.Rows[0]["idprof"].ToString();
            txtna.Text = dt.Rows[0]["nomape"].ToString();
            txtdnimp.Text = dt.Rows[0]["dni"].ToString();
            txttelmp.Text = dt.Rows[0]["tel"].ToString();
            dateTimePicker1.Text = dt.Rows[0]["fenac"].ToString();
            txtdirmp.Text = dt.Rows[0]["direccion"].ToString();
            txtnromp.Text = dt.Rows[0]["nro"].ToString();
            txtmail.Text = dt.Rows[0]["mail"].ToString();
            obHora(hora);
            obdia(dia);
            obactm(ac);
            comloc(md);

        }
        public static bool validar(string email)
        {
            string expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, String.Empty).Length == 0)
                { return true; }
                else
                { return false; }
            }
            else
            { return false; }
        }
        private void obactm(string idmp)
        {
            string acmSQL = ApTables.datosAct();
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(acmSQL, cadCn);

            if(dt!= null)
            {
                cbactmp.ValueMember = "idactividad";
                cbactmp.DisplayMember = "nomact";
                cbactmp.DataSource = dt;
            }
            cbactmp.SelectedValue = idmp;
        }
        private void obdia(string iddia)
        {
            string acmSQL = ApTables.obtenerDias();
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(acmSQL, cadCn);

            if (dt != null)
            {
                cbdiam.ValueMember = "iddia";
                cbdiam.DisplayMember = "Dias";
                cbdiam.DataSource = dt;
            }
            cbactmp.SelectedValue = iddia;
        }
        private void obHora(string idh)
        {
            string hSQL = ApTables.obtenerhoras();
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(hSQL, cadCn);

            if (dt != null)
            {
                cbhorpm.ValueMember = "idhorp";
                cbhorpm.DisplayMember = "Horarios";
                cbhorpm.DataSource = dt;
            }
            cbhorpm.SelectedValue = idh;
        }
        private bool Validarmod()
        {
            if (txtna.Text == "")
            {
                MessageBox.Show("Debe ingresar un nombre y apellido");
                txtna.Focus();
                return false;
            }
            if (txtdnimp.Text == "")
            {
                MessageBox.Show("Debe ingresar el DNI");
                txtdnimp.Focus();
                return false;
            }
            if (txttelmp.Text == "")
            {
                MessageBox.Show("Debe ingresar un telefono");
                txttelmp.Focus();
                return false;
            }
            if (txtdirmp.Text == "")
            {
                MessageBox.Show("Debe ingresar una direcciòn");
                txtdirmp.Focus();
                return false;
            }
            if (txtnromp.Text == "")
            {
                MessageBox.Show("Debe ingresar el numero de la calle");
                txtnromp.Focus();
                return false;
            }
            if (txtmail.Text == "")
            {
                MessageBox.Show("Debe ingresar su mail");
                txtmail.Focus();
                return false;
            }
            return true;
        }
        //private void ValidacionHoras()
        //{
        //    string hSQL = "select * from gimnasio.horariosprof  where idhorp not in (select idhorario from profesionales where iddia =" + cbdiam.SelectedValue + ")";
        //    dt = new DataTable();
        //    objDatos = new DatosMySQL();

        //    dt = objDatos.traerDataTable(hSQL, cadCn);

        //    if (dt != null)
        //    {
        //        cbhorpm.ValueMember = "idhorp";
        //        cbhorpm.DisplayMember = "Horarios";
        //        cbhorpm.DataSource = dt;
        //    }
        //}

        private void cbprov_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRowView dr = (DataRowView)cbprov.SelectedItem;
            int id = Convert.ToInt32(dr[0]);

            string lpmSQL = ApTables.locxprov(id.ToString());
            dt = new DataTable();
            objDatos = new DatosMySQL();
            dt = objDatos.traerDataTable(lpmSQL, cadCn);

            cbloc.ValueMember = "idloc";
            cbloc.DisplayMember = "nomloc";
            cbloc.DataSource = dt;
        }
        private void comboprov()
        {
            string provmSQL = ApTables.datosProv();
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(provmSQL, cadCn);
                        
                cbprov.ValueMember = "idprov";
                cbprov.DisplayMember = "nomprov";
                cbprov.DataSource = dt;
            
        }
        private void comloc(string iddm)
        {
            string locmSQL = ApTables.datosLoc();
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(locmSQL, cadCn);
            if (dt != null)
            {
                cbloc.ValueMember = "idloc";
                cbloc.DisplayMember = "nomloc";
                cbloc.DataSource = dt;
            }
            cbloc.SelectedValue = iddm;
        }
        private void modificarprof()
        {
            var fechnac = (Convert.ToDateTime(dateTimePicker1.Text).ToString("yyyy-MM-dd"));
            string mprSQL = "update gimnasio.profesionales set nomape = '" + txtna.Text + "', dni= " + txtdnimp.Text + ",tel =" + txttelmp.Text +
                ",fenac= '" + fechnac + "', direccion ='" + txtdirmp.Text + "', nro=" + txtnromp.Text + ", mail='" + txtmail.Text +
                "', idact=" + Convert.ToInt32(cbactmp.SelectedValue) + ", iddia=" + Convert.ToInt32(cbdiam.SelectedValue) + ", idhorario=" + Convert.ToInt32(cbhorpm.SelectedValue) + ", loc=" + Convert.ToInt32(cbloc.SelectedValue) + 
                " where idprof = " + txtidmodprof.Text + ";";
            objDatos = new DatosMySQL();

            valor = objDatos.ejecutarSQL(mprSQL, cadCn);
        }

        private void txtmail_Leave(object sender, EventArgs e)
        {
            validar(txtmail.Text);
        }

        private void txthormp_Leave(object sender, EventArgs e)
        {
            validar(cbhorpm.Text);
        }

        private void btmodmprof_Click(object sender, EventArgs e)
        {
            if (Validarmod() == true)
            {
                modificarprof();
                this.Close();
            }
        }

        private void txtdnimp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txttelmp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtnromp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void cbdiam_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ValidacionHoras();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
