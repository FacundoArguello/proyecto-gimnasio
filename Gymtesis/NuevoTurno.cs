﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//
using ConectorBD;
using System.Configuration;

namespace Gymtesis
{
    public partial class NuevoTurno : Form
    {
        DatosMySQL objDatos;
        DataTable dt;      
        string cadCn;
        int valor;
        public string dni { get; set; }
        public NuevoTurno()
        {
            InitializeComponent();
        }

        private void NuevoTurno_Load(object sender, EventArgs e)
        {
            cadCn = ConfigurationManager.ConnectionStrings["MYSQL"].ConnectionString;
            if (btnguardar.Visible == false)
            {
                turnoxnewturn();
            } 
        }
        private void agregarturno()
        {
            var fechturn = (Convert.ToDateTime(dtpt.Text).ToString("yyyy-MM-dd"));
            string turnASQL = "insert into gimnasio.turnos (dniclien, nomape, fecha, hora)" +
                "values (" + txtncliente.Text + ",'" + textBox1.Text + "','" + fechturn + "','" + Convert.ToInt32(cbhor.SelectedValue) + "')";
            objDatos = new DatosMySQL();

            valor = objDatos.ejecutarSQL(turnASQL, cadCn);
        }
        private void btnguardar_Click(object sender, EventArgs e)
        {
            if (validart() == true)
            {
                agregarturno();
                this.Close();
            }
        }
        private bool validart()
        {
            if (txtncliente.Text == "")
            {
                MessageBox.Show("Debe ingresar El numero de dni del cliente");
                txtncliente.Focus();
                return false;
            }
            if (textBox1.Text == "")
            {
                MessageBox.Show("Debe ingresar el Nombre del cliente");
                textBox1.Focus();
                return false;
            }
            return true;
        }
        public void turnoxnewturn()
        {
            string ntSQL = "select * from gimnasio.turnos where dniclien= " + dni;
            objDatos = new DatosMySQL();
            dt = new DataTable();

            dt = objDatos.traerDataTable(ntSQL, cadCn);

            if (dt == null) return;
            {
                string cb = dt.Rows[0]["hora"].ToString();
                txtncliente.Text = dt.Rows[0]["dniclien"].ToString();
                textBox1.Text = dt.Rows[0]["nomape"].ToString();
                dtpt.Text = dt.Rows[0]["fecha"].ToString();
                combohora(cb);
            }
        }
        public void modificarturno()
        {
            var fechturnm = (Convert.ToDateTime(dtpt.Text).ToString("yyyy-MM-dd"));
            string mturnSQL = "update gimnasio.turnos set dniclien=" + txtncliente.Text + ", nomape='" + textBox1.Text + "', fecha='" + fechturnm +
                "', hora='" + Convert.ToInt32(cbhor.SelectedValue) + "' where dniclien=" + txtncliente.Text + ";";
            objDatos = new DatosMySQL();

            valor = objDatos.ejecutarSQL(mturnSQL, cadCn);
        }

        private void btnmod_Click(object sender, EventArgs e)
        {
            modificarturno();
            this.Close();
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtncliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
      
        private void dtpt_ValueChanged(object sender, EventArgs e)
        {
            var dtp = sender as DateTimePicker;
            
            if (dtp.Value.DayOfWeek == DayOfWeek.Saturday || dtp.Value.DayOfWeek == DayOfWeek.Sunday)
            {
                MessageBox.Show("No es posible seleccionar el día sábado o domingo");
                //Selecciona el último viernes
                dtp.Value = dtp.Value.AddDays(dtp.Value.DayOfWeek == DayOfWeek.Sunday ? -2 : -1);
                dtp.Focus();
            }
            obxhor();                            
        }
        private void combohora(string ida)
        {
            var feval = (Convert.ToDateTime(dtpt.Text).ToString("yyyy-MM-dd"));
            string horSQL = "select * from gimnasio.horario where idhor not in (select hora from turnos where fecha ='" + feval + "')"; 
            objDatos = new DatosMySQL();
            dt = new DataTable();

            dt = objDatos.traerDataTable(horSQL, cadCn);

            if(dt != null)
            {
                cbhor.ValueMember = "idhor";
                cbhor.DisplayMember = "rango";
                cbhor.DataSource = dt;
            }
            cbhor.SelectedValue = ida;
        }
        private void obxhor()
        {
            var fval = (Convert.ToDateTime(dtpt.Text).ToString("yyyy-MM-dd"));
            string hSQL = "select * from gimnasio.horario where idhor not in (select hora from turnos where fecha ='" + fval + "')";
            dt = new DataTable();
            objDatos = new DatosMySQL();

            dt = objDatos.traerDataTable(hSQL, cadCn);

            if (dt != null)
            {
                cbhor.ValueMember = "idhor";
                cbhor.DisplayMember = "rango";
                cbhor.DataSource = dt;
            }
            
        }
        private void buscardni()
        {
            if (txtncliente.Text == "") return;
            string dniSQL = "select dniclien from turnos where dniclien =" + txtncliente.Text;
            dt = new DataTable();
            objDatos = new DatosMySQL();
            dt = objDatos.traerDataTable(dniSQL, cadCn);

            if (dt == null) return;

            string dniig = dt.Rows[0]["dniclien"].ToString();
            if (dniig == txtncliente.Text)
            {
                try
                {
                    MessageBox.Show("Numero de documento ingresado anteriormente en el sistema");
                    txtncliente.Text = "";
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void cbhor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtncliente_Validating(object sender, CancelEventArgs e)
        {
            buscardni();
        }
    }
}
