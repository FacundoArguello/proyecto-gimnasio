﻿namespace Gymtesis
{
    partial class Turnero
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Turnero));
            this.btcerrart = new System.Windows.Forms.Button();
            this.btelimt = new System.Windows.Forms.Button();
            this.btmodt = new System.Windows.Forms.Button();
            this.dgvturnos = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvturnos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btcerrart
            // 
            this.btcerrart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btcerrart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btcerrart.Image = ((System.Drawing.Image)(resources.GetObject("btcerrart.Image")));
            this.btcerrart.Location = new System.Drawing.Point(468, 31);
            this.btcerrart.Name = "btcerrart";
            this.btcerrart.Size = new System.Drawing.Size(109, 67);
            this.btcerrart.TabIndex = 11;
            this.btcerrart.Text = "Cerrar";
            this.btcerrart.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btcerrart.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btcerrart.UseVisualStyleBackColor = true;
            this.btcerrart.Click += new System.EventHandler(this.btcerrart_Click);
            // 
            // btelimt
            // 
            this.btelimt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btelimt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btelimt.Image = ((System.Drawing.Image)(resources.GetObject("btelimt.Image")));
            this.btelimt.Location = new System.Drawing.Point(313, 31);
            this.btelimt.Name = "btelimt";
            this.btelimt.Size = new System.Drawing.Size(149, 67);
            this.btelimt.TabIndex = 10;
            this.btelimt.Text = "Eliminar Turno";
            this.btelimt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btelimt.UseVisualStyleBackColor = true;
            this.btelimt.Click += new System.EventHandler(this.btelimt_Click);
            // 
            // btmodt
            // 
            this.btmodt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btmodt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmodt.Image = ((System.Drawing.Image)(resources.GetObject("btmodt.Image")));
            this.btmodt.Location = new System.Drawing.Point(158, 31);
            this.btmodt.Name = "btmodt";
            this.btmodt.Size = new System.Drawing.Size(149, 67);
            this.btmodt.TabIndex = 9;
            this.btmodt.Text = "Modificar Turno";
            this.btmodt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btmodt.UseVisualStyleBackColor = true;
            this.btmodt.Click += new System.EventHandler(this.btmodt_Click);
            // 
            // dgvturnos
            // 
            this.dgvturnos.AllowUserToAddRows = false;
            this.dgvturnos.AllowUserToDeleteRows = false;
            this.dgvturnos.AllowUserToOrderColumns = true;
            this.dgvturnos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvturnos.Location = new System.Drawing.Point(12, 79);
            this.dgvturnos.Name = "dgvturnos";
            this.dgvturnos.ReadOnly = true;
            this.dgvturnos.Size = new System.Drawing.Size(526, 228);
            this.dgvturnos.TabIndex = 8;
            this.dgvturnos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvturnos_CellContentClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(22, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 24);
            this.label1.TabIndex = 7;
            this.label1.Text = "Turnos";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(3, 31);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(149, 67);
            this.button1.TabIndex = 12;
            this.button1.Text = "Agregar Turno";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(227)))), ((int)(((byte)(4)))));
            this.panel1.Controls.Add(this.btcerrart);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btelimt);
            this.panel1.Controls.Add(this.btmodt);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 313);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(582, 101);
            this.panel1.TabIndex = 13;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(227)))), ((int)(((byte)(4)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(574, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(8, 313);
            this.panel2.TabIndex = 14;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(227)))), ((int)(((byte)(4)))));
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Location = new System.Drawing.Point(477, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 81);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // Turnero
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(582, 414);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dgvturnos);
            this.Controls.Add(this.label1);
            this.Name = "Turnero";
            this.Text = "Turnero";
            this.Load += new System.EventHandler(this.Turnero_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvturnos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btcerrart;
        private System.Windows.Forms.Button btelimt;
        private System.Windows.Forms.Button btmodt;
        public System.Windows.Forms.DataGridView dgvturnos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}