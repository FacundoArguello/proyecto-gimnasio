﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//
using ConectorBD;
using System.Configuration;

namespace Gymtesis
{
    public partial class Turnero : Form
    {
        DatosMySQL objDatos;
        DataTable dt;
        string cadCn;

        public Turnero()
        {
            InitializeComponent();
        }

        private void Turnero_Load(object sender, EventArgs e)
        {
            cadCn = ConfigurationManager.ConnectionStrings["MYSQL"].ConnectionString;

            bindingSource1.DataSource = dt;
            dgvturnos.DataSource = bindingSource1;

            borfechantiguas();
            obturnero();
        }
        private void obturnero()
        {
            string turSQL = ApTables.BDturnero();
            objDatos = new DatosMySQL();
            dt = new DataTable();

            dt = objDatos.traerDataTable(turSQL, cadCn);

            if (dt == null || dt.Rows.Count == 0)
                bindingSource1.DataSource = null;
            else
                bindingSource1.DataSource = dt;
        }

        private void btelimt_Click(object sender, EventArgs e)
        {
            var dni = dgvturnos.CurrentRow.Cells[0].Value;
            var nom = dgvturnos.CurrentRow.Cells[1].Value;
            var fecha = dgvturnos.CurrentRow.Cells[2].Value;
            var hora = dgvturnos.CurrentRow.Cells[3].Value;
            string eturSQL = ApTables.BDelimturnos(dni.ToString());
            objDatos = new DatosMySQL();
            int valordev = -1;

            string msg = "Desea eliminar el turno?" + Environment.NewLine + "DNI: " + dni + Environment.NewLine + "Cliente: " + nom + Environment.NewLine + "Fecha: " + fecha + Environment.NewLine + "Hora :" + hora;
            DialogResult okt = MessageBox.Show(msg, "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if(okt== System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    valordev = objDatos.ejecutarSQL(eturSQL, cadCn);
                }
                catch(SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                if (valordev > 0)
                    obturnero();
            }
        }
        private void borfechantiguas()
        {
            string febSQL = ApTables.BDBORRFECH();
            int val= 1;
            objDatos = new DatosMySQL();

            val = objDatos.ejecutarSQL(febSQL, cadCn);
        }
        private void button1_Click(object sender, EventArgs e)
        {            
            NuevoTurno nt = new NuevoTurno();
            nt.btnmod.Visible = false;            
            nt.ShowDialog();
            obturnero();


        }

        private void btmodt_Click(object sender, EventArgs e)
        {
            NuevoTurno ntm = new NuevoTurno();
            ntm.dni = dgvturnos.CurrentRow.Cells[0].Value.ToString();
            ntm.btnguardar.Visible = false;
            ntm.ShowDialog();
            obturnero();

        }

        private void btcerrart_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvturnos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
